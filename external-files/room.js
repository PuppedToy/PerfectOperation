var numberOfRooms = 0;

function Room(height, width, cellSize) {

	this.height = height;
	this.width = width;
	this.cellSize = cellSize;
	this.cells = [];
	this.elements = []; // I'm thinking about making divisor points (obstacles)
	this.importantWalls = [];
	this.adjacentRooms = [];
	for(var i=0; i < 6; i++) {
		this.adjacentRooms[i] = null;
	}

	this.roads = [];
	this.id = numberOfRooms++;

	this.players = [];
	
	for(var i = 0; i < this.width; i++) { // initially a room is full normal without important walls
		this.cells[i] = [];
		for(var j = 0; j < this.height; j++) {
			this.cells[i][j] = new Cell(new Position(i, j, this.id), this);
		}
	}

	for(var i in this.cells) {
		for(var j in this.cells[i]) {
			if(j < this.height-1) this.cells[i][j].connect(this.cells[i][parseInt(j)+1]);
			if(i < this.width-1) this.cells[i][j].connect(this.cells[parseInt(i)+1][j]);
		}
	}

}

Room.prototype.equals = function(room) {
	return this.id == room.id;
}

Room.prototype.getRoom = function(direction) {
	return this.adjacentRooms[direction];
}

Room.prototype.setRoom = function(room, direction) {
	this.adjacentRooms[direction] = room;
}

Room.prototype.getCell = function(position) {
	return this.cells[position.x][position.y];
}

Room.prototype.addElement = function(position, typeId) {
	this.getCell(position).setElement(typeId);
}

Room.prototype.removeElement = function(position) {
	this.getCell(position).removeElement();
}

Room.prototype.getElement = function(typeId) {
	for(var i in this.elements) {
		if(this.elements[i].type.name == typeId) return this.elements[i];
	}
	return null;
}

Room.prototype.getRoomDirection = function(roomId) {
	for(var i = 0; i < this.adjacentRooms.length; i++) {
		if(this.adjacentRooms[i] != null && this.adjacentRooms[i].id == roomId) return i;
	}
	return null;
}

Room.prototype.addPlayer = function(player) {
	this.players.push(player);
	this.getCell(player.position).setCharacterOver(player);
}

Room.prototype.removePlayer = function(player) {
	for(var i in this.players) {
		if(player.id == this.players[i].id) {
			this.players.splice(i, 1);
			return;
		}
	}
}

Room.prototype.hasPlayer = function(player) {
	for(var i in this.players) {
		if(this.players[i].id == player.id) return true;
	}
	return false;
}

Room.prototype.getPossibleEntrance = function() {
	var possibilities = [];

	if(this.getRoom(NORTH) == null) possibilities.push(NORTH);
	if(this.getRoom(EAST) == null) possibilities.push(EAST);
	if(this.getRoom(WEST) == null) possibilities.push(WEST);
	if(this.getRoom(SOUTH) == null) possibilities.push(SOUTH);

	if(possibilities.length == 0) return null;
	else return possibilities[randomNum(0,possibilities.length-1)];
}

Room.prototype.setCellElement = function(position, typeId) {
	this.getCell(position).setElement(typeId);
}

Room.prototype.setCharacterOverCell = function(position, character) {
	this.cells[position.x][position.y].setCharacterOver(character);
};

Room.prototype.build = function(complexityValue) {
	
	for(var i in this.elements) {
		var error = this.getCell(this.elements[i].position).roundWithWalls();
		if(error) return true;
	}

	this.finishWalls(sample(sample(this.cells)));
	if(!this.cells[0][0].hasPath(NORTH) && !this.cells[0][0].hasPath(EAST) && !this.cells[0][0].hasPath(WEST) && !this.cells[0][0].hasPath(SOUTH)) return true;

	//this.makeRoadMemories();

	return false;

}

Room.prototype.connect = function(room, direction) {

	this.setRoom(room, direction);
	room.setRoom(this, oppositeDirection(direction));

}

Room.prototype.map = function() {
	var aux = "";
	for(var i in this.cells) {
		for(var j in this.cells[i]) {
			aux += this.cells[i][j].typeId + " ";
		}
		aux += "<br>";
	}
	return aux;
}

Room.prototype.toString = function() {
	res = "Room " + this.id;
	/*for(var i in this.cells) {
		for(var j in this.cells[i]) {
			res += this.cells[i][j] + " ";
		}
		res += "\n";
	}*/
	return res;
}

Room.prototype.draw = function() {

	canvas.beginPath();
	canvas.lineWidth="3";
	canvas.strokeStyle="black";
	canvas.rect(2, 2, cellSize*this.height, cellSize*this.width);
	canvas.stroke();

	for (var i in this.cells) {
		for (var j in this.cells) {
			this.cells[i][j].draw();
			this.cells[i][j].drawElement();
		}
	}

}

Room.prototype.drawElements = function() {
	for(var i in this.cells) {
		for(var j in this.cells) {
			this.cells[i][j].drawElement();
		}
	}
}

Room.prototype.addImportantWall = function(position, direction) {

	this.getCell(position).setOrUnsetImportantWall(direction);

}

Room.prototype.isImportantWall = function(position, direction) {

	return this.getCell(position).hasImportantWall(direction);

}

Room.prototype.finishWalls = function(start) {

	var visited = [start];
	start.visited = true;
	var visitedPossible = [start];
	var possibilities = [];
	var present, room = this;
	var road = null;

	checkVisitedPossibilities();

	while(visitedPossible.length > 0) {

		present = visitedPossible[randomNum(0,visitedPossible.length-1)];
		possibilities = getPossibleDirections(present);

		// new road :D
		this.roads.push(new Road());
		road = this.roads[this.roads.length-1];

		// If the cell is the very first one of the room
		if(present.road_belonged == null) present.setToRoad(road);

		do {
			var dirAux = possibilities[randomNum(0,possibilities.length-1)];
			present.setOrUnsetPath(dirAux);
			visit(present, dirAux);
			present = visited[visited.length-1];
			possibilities = getPossibleDirections(present);
		} while(possibilities.length > 0 && Math.random() > 2/(this.width*this.height));

		checkVisitedPossibilities();

	}

	function isPossible(cell, direction) {
		return cell.getCell(direction) != null && !cell.getCell(direction).visited && !room.isImportantWall(cell.position, direction);
	}

	function getPossibleDirections(cell) {

		var result = [];

		for(var i = 0; i < 4; i++) {
			if(isPossible(cell, i)) result.push(i);
		}

		return result;

	}

	function visit(cell, direction) {
		var newCell = cell.getCell(direction);
		visited.push(newCell);
		newCell.visited = true;
		visitedPossible.push(newCell);

		// For roads

		newCell.setToRoad(road);
		// If i'm in the same road, we continue it
		if(cell.road_belonged.id == road.id) road.path.push(direction);
		// Else, i'm in a new one, so I dont have to add it to the path, but i have to generate the unions
		else {
			cell.road_belonged.addUnion(cell.road_code, road, direction);
			road.addUnion(newCell.road_code, cell.road_belonged, oppositeDirection(direction));
		}
	}

	function checkVisitedPossibilities() {
		for(var k=0; k < visitedPossible.length; k++) {
			if(getPossibleDirections(visitedPossible[k]).length == 0) {
				visitedPossible.splice(k,1);
				k--;
			}
		}
	}

}

Room.prototype.makeRoadMemories = function() {
	for(var i in this.roads) {
		this.roads[i].makeMemories(this.roads);
	}
}

Room.prototype.weakestPlayers = function(player) {
	var weakest = [];
	var weakLvl = 99;

	for(var i in this.players) {
		if(this.players[i].team != player.team) {
			var playerLvl = ((this.players[i].weapon == null) ? 0 : this.players[i].weapon.level);
			if(playerLvl == weakLvl) weakest.push(this.players[i]);
			else if(playerLvl < weakLvl) {
				weakLvl = playerLvl;
				weakest = [this.players[i]];
			}
		}
	}

	return weakest;
}

Room.prototype.strongestWeapons = function() {

	var strongest = [];
	var strongLvl = 0;

	for(var i in this.elements) {
		if(this.elements[i].type.isWeapon != undefined && this.elements[i].type.name != "Bomb") {
			var weaponLvl = this.elements[i].type.level;
			if(weaponLvl == strongLvl) strongest.push(this.elements[i]);
			else if(weaponLvl > strongLvl) {
				strongLvl = weaponLvl;
				strongest = [this.elements[i]];
			}
		}
	}

	return strongest;
}

Room.prototype.valuestItems = function() {

	var valuest = [];
	var value = 0;

	for(var i in this.elements) {
		if(this.elements[i].type.isValuableItem != undefined) {
			var itemValue = this.elements[i].type.value;
			if(itemValue == value) valuest.push(this.elements[i]);
			else if(itemValue > value) {
				value = itemValue;
				valuest = [this.elements[i]];
			}
		}
	}

	return valuest;
}

Room.prototype.getWeapons = function() {
	var weapons = [];

	for(var i in this.elements) {
		if(this.elements[i].type.isWeapon != undefined && this.elements[i].type.name != "Bomb") {
			weapons.push(this.elements[i]);
		}
	}

	return weapons;
}

Room.prototype.getPassages = function() {
	var passages = [];

	for(var i in this.elements) {
		if(this.elements[i].type.name.indexOf("passage") > -1) {
			passages.push(this.elements[i]);
		}
	}

	return passages;
}

Room.prototype.nakedBases = function(team, myCastle) {

	var bases = [];

	for(var i in this.elements) {
		var elem = this.elements[i];
		if(elem.type.name.indexOf("Base") > -1 && elem.type.name != "Baseball_Bat" && elem.type.name != "Base" + team) bases.push(elem);
	}

	var nakedBases = [];

	for(var i in bases) {
		if(bases[i].type.vulnerable(myCastle.players, myCastle.playersForTeam)) nakedBases.push(bases[i]);
	}

	return nakedBases;

}

Room.prototype.clearWalls = function() {
	for(var i in this.cells) {
		for(var j in this.cells[i]) {
			var c = this.cells[i][j];
			for(var k = 0; k < 4; k++) {
				if(c.getCell(k) != null) {
					c.setOrUnsetImportantWall(k, false);
					c.setOrUnsetPath(k, true);	
				} 

			}
		}
	}
}

Room.prototype.getCode = function() {
	var res = "";
	for(var i = 0; i < this.cells.length; i++) {
		for(var j = 0; j < this.cells[i].length; j++) {
			res += this.cells[i][j].getCode();
			if(i < this.cells.length || j < this.cells[i].length) res += "#";
		}
	}
	res += "^";
	for(var i = 0; i < this.roads.length; i++) {
		res += this.roads.getCode();
		if(i < this.roads.length-1) res += "#";
	}
	res += "^";
	for(var i = 0; i < this.adjacentRooms.length; i++) {
		res += ((this.adjacentRooms[i] == null) ? "null" : this.adjacentRooms[i].id);
		if(i < this.adjacentRooms.length-1) res += "#"
	}
	return res;
}

function ImportantWall (cell, direction) {
	this.position1 = cell.position;
	this.direction1 = direction;
	this.position2 = cell.getCell(direction);
	this.direction2 = oppositeDirection(direction);
}

ImportantWall.prototype.contains = function(position, direction) {
	return (this.position1.equals(position) && this.direction1 === direction) || (this.position2.equals(position) && this.direction2 === direction);
}
