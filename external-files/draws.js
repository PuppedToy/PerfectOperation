elementtypes[searchElement("key")].draw = function(element) {

	playercanvas.fillStyle = element.color;
	playercanvas.lineWidth = "" + cellSizePart(0.15);
	playercanvas.beginPath();
	playercanvas.arc(4 + element.position.x*cellSize + cellSizePart(0.75), 4 + element.position.y*cellSize + cellSizePart(0.25), cellSizePart(0.175), 0, 2*Math.PI);
	playercanvas.stroke();

	playercanvas.lineWidth = "1";
	playercanvas.beginPath();
	playercanvas.moveTo(4 + element.position.x*cellSize + cellSizePart(0.575), 4 + element.position.y*cellSize + cellSizePart(0.325));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.075), 4 + element.position.y*cellSize + cellSizePart(0.825));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.175), 4 + element.position.y*cellSize + cellSizePart(0.925));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.25), 4 + element.position.y*cellSize + cellSizePart(0.875));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.375), 4 + element.position.y*cellSize + cellSizePart(1));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.5), 4 + element.position.y*cellSize + cellSizePart(0.875));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.375), 4 + element.position.y*cellSize + cellSizePart(0.75));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.425), 4 + element.position.y*cellSize + cellSizePart(0.675));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.575), 4 + element.position.y*cellSize + cellSizePart(0.825));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.675), 4 + element.position.y*cellSize + cellSizePart(0.725));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.575), 4 + element.position.y*cellSize + cellSizePart(0.575));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.675), 4 + element.position.y*cellSize + cellSizePart(0.475));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.575), 4 + element.position.y*cellSize + cellSizePart(0.325));
	playercanvas.fill();

}

elementtypes[searchElement("passage" + UP)].draw = function(element) {

	playercanvas.strokeStyle = "black";
	playercanvas.fillStyle = "#61210B";
	var x = 4 + element.position.x*cellSize + cellSizePart(0.75);
	var y = 4 + element.position.y*cellSize;
	// No funciona en absoluto :(
	playercanvas.strokeRect(4 + element.position.x*cellSize + cellSizePart(0.5), 4 + element.position.y*cellSize + cellSizePart(0.5), cellSizePart(0.5), cellSizePart(0.5));
	playercanvas.fillRect(4 + element.position.x*cellSize + cellSizePart(0.5), 4 + element.position.y*cellSize + cellSizePart(0.5), cellSizePart(0.5), cellSizePart(0.5));

	playercanvas.beginPath();


	var steps = 6;
	for(var i = 0; i < steps; i++) {

		playercanvas.beginPath();
		playercanvas.moveTo(x,y);
		playercanvas.lineTo(x - cellSizePart(0.1), y);
		playercanvas.lineTo(x + cellSizePart(0.15), y + cellSizePart(0.5));
		playercanvas.lineTo(x + cellSizePart(0.25), y + cellSizePart(0.5));
		playercanvas.lineTo(x,y);
		playercanvas.fill();
		playercanvas.stroke();

		if(i < 5) {
			playercanvas.beginPath();
			x -= cellSizePart(0.1);
			playercanvas.moveTo(x, y);
			playercanvas.lineTo(x, y + cellSizePart(0.1));
			playercanvas.lineTo(x + cellSizePart(0.25), y + cellSizePart(0.6));
			playercanvas.lineTo(x, y);
			y += cellSizePart(0.1);
			playercanvas.fill();
			playercanvas.stroke();
		}
	}
}

elementtypes[searchElement("passage" + DOWN)].draw = function(element) {

	playercanvas.strokeStyle = "black";
	playercanvas.fillStyle = "#61210B";
	playercanvas.beginPath();
	playercanvas.moveTo(4 + element.position.x*cellSize, 4 + element.position.y*cellSize + cellSizePart(0.25));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.5), 4 + element.position.y*cellSize + cellSizePart(0.25));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.75), 4 + element.position.y*cellSize + cellSizePart(0.75));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.25), 4 + element.position.y*cellSize + cellSizePart(0.75));
	playercanvas.lineTo(4 + element.position.x*cellSize, 4 + element.position.y*cellSize + cellSizePart(0.25));
	playercanvas.fill();
	playercanvas.stroke();

	playercanvas.strokeStyle = "black";
	playercanvas.fillStyle = "grey";

	var x = 4 + element.position.x*cellSize + cellSizePart(0.6);
	var y = 4 + element.position.y*cellSize + cellSizePart(0.25);
	var ymax = 4 + element.position.y*cellSize + cellSizePart(0.75);
	var xresta = 0;

	var steps = 4;
	playercanvas.beginPath();
	playercanvas.moveTo(x, y);
	for(var i = 0; i < steps; i++) {

		x -= cellSizePart(0.1);
		playercanvas.lineTo(x, y);
		y += cellSizePart(0.1);
		playercanvas.lineTo(x, y);

	}
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.25), 4 + element.position.y*cellSize + cellSizePart(0.75));
	playercanvas.lineTo(4 + element.position.x*cellSize, 4 + element.position.y*cellSize + cellSizePart(0.25));
	playercanvas.fill();
	playercanvas.stroke();

	

}

elementtypes[searchElement("passage" + NORTH)].draw = function(element) {

	var gradient = playercanvas.createLinearGradient(4 + element.position.x*cellSize, 4 + element.position.y*cellSize , 4 + element.position.x*cellSize,  4 + element.position.y*cellSize + cellSizePart(0.75));
	gradient.addColorStop(0,"yellow");
	gradient.addColorStop(1,"white");
	playercanvas.fillStyle = gradient;
	playercanvas.fillRect(4 + element.position.x*cellSize, 4 + element.position.y*cellSize , cellSize - 2, cellSize - 2);

}

elementtypes[searchElement("passage" + SOUTH)].draw = function(element) {

	var gradient = playercanvas.createLinearGradient(4 + element.position.x*cellSize, 4 + element.position.y*cellSize + cellSize, 4 + element.position.x*cellSize,  4 + element.position.y*cellSize + cellSizePart(0.25));
	gradient.addColorStop(0,"yellow");
	gradient.addColorStop(1,"white");
	playercanvas.fillStyle = gradient;
	playercanvas.fillRect(4 + element.position.x*cellSize, 4 + element.position.y*cellSize , cellSize - 2, cellSize - 2);

}

elementtypes[searchElement("passage" + EAST)].draw = function(element) {

	var gradient = playercanvas.createLinearGradient(4 + element.position.x*cellSize + cellSize, 4 + element.position.y*cellSize, 4 + element.position.x*cellSize + cellSizePart(0.25),  4 + element.position.y*cellSize);
	gradient.addColorStop(0,"yellow");
	gradient.addColorStop(1,"white");
	playercanvas.fillStyle = gradient;
	playercanvas.fillRect(4 + element.position.x*cellSize, 4 + element.position.y*cellSize , cellSize - 2, cellSize - 2);

}

elementtypes[searchElement("passage" + WEST)].draw = function(element) {

	var gradient = playercanvas.createLinearGradient(4 + element.position.x*cellSize, 4 + element.position.y*cellSize, 4 + element.position.x*cellSize + cellSizePart(0.75),  4 + element.position.y*cellSize);
	gradient.addColorStop(0,"yellow");
	gradient.addColorStop(1,"white");
	playercanvas.fillStyle = gradient;
	playercanvas.fillRect(4 + element.position.x*cellSize, 4 + element.position.y*cellSize , cellSize - 2, cellSize - 2);

}

elementtypes[searchElement("Bomb_Exploding3")].draw = function(element) {
	var img = $("#Bomb_Exploding3")[0];
	playercanvas.drawImage(img, 6+element.position.x*cellSize - cellSize, 6+element.position.y*cellSize - cellSize, (cellSize-2)*3, (cellSize-2)*3);
}

obstacletypes[searchObstacle("keydoor")].draw = function(element) {

	playercanvas.fillStyle = element.color;
	playercanvas.lineWidth = "1";
	playercanvas.fillRect(4 + element.position.x*cellSize + cellSizePart(0.25), 4 + element.position.y*cellSize,cellSizePart(0.5),cellSize - 2);
	playercanvas.fillStyle = "yellow";
	playercanvas.beginPath();
	playercanvas.arc(4 + element.position.x*cellSize + cellSizePart(0.675), 4 + element.position.y*cellSize + cellSizePart(0.5),cellSizePart(0.075), 0, 2*Math.PI);
	playercanvas.fill();

}

Base.prototype.draw = function(element) {

	if(castillo != undefined && this.vulnerable(castillo.players, castillo.playersForTeam)) playercanvas.strokeStyle = "red";
	else playercanvas.strokeStyle = "black";

	playercanvas.fillStyle = this.player.color;
	playercanvas.beginPath();
	playercanvas.moveTo(4 + element.position.x*cellSize + cellSizePart(0.1), 4 + element.position.y*cellSize + cellSizePart(0.9));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.5), 4 + element.position.y*cellSize + cellSizePart(0.9));
	playercanvas.stroke();
	playercanvas.beginPath();
	playercanvas.moveTo(4 + element.position.x*cellSize + cellSizePart(0.3), 4 + element.position.y*cellSize + cellSizePart(0.9));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.3), 4 + element.position.y*cellSize + cellSizePart(0.1));
	playercanvas.stroke();
	playercanvas.beginPath();
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.9), 4 + element.position.y*cellSize + cellSizePart(0.3));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.3), 4 + element.position.y*cellSize + cellSizePart(0.5));
	playercanvas.lineTo(4 + element.position.x*cellSize + cellSizePart(0.3), 4 + element.position.y*cellSize + cellSizePart(0.1));
	playercanvas.stroke();
	playercanvas.fill();

	var x = 0.03;
	var y = 0.03;
	for(var i = 0; i < this.items.length && i < 9; i++) {
		img = $("#" + this.items[i].name)[0];
		playercanvas.drawImage(img, 4 + element.position.x*cellSize + cellSizePart(x), 4 + element.position.y*cellSize + cellSizePart(y), cellSizePart(0.3), cellSizePart(0.3));
		x += 0.33;
		if(x > 1) {
			x = 0.03;
			y += 0.33;
		}
	}

}

function drawImage (position, name) {
	var img = $("#" + name)[0];
	playercanvas.drawImage(img, 4+position.x*cellSize, 4+position.y*cellSize, cellSize-2, cellSize-2);
}

function drawImageRot(ctx,img,x,y,width,height,deg){

    //Convert degrees to radian 
    var rad = deg * Math.PI / 180;

    //Set the origin to the center of the image
    ctx.translate(x + width / 2, y + height / 2);

    //Rotate the canvas around the origin
    ctx.rotate(rad);

    //draw the image    
    ctx.drawImage(img,width / 2 * (-1),height / 2 * (-1),width,height);

    //reset the canvas  
    ctx.rotate(rad * ( -1 ) );
    ctx.translate((x + width / 2) * (-1), (y + height / 2) * (-1));
}

var shapes = ["blank", "topcircle", "crossline", "concentric", "triforce", "nazi", "horns"];

function PlayerSkin(main_color, secondary_color, shape) {

	if(arguments.length == 0) {
		this.main_color = "#" + Math.random().toString(16).slice(2, 8);
		this.secondary_color = "#" + Math.random().toString(16).slice(2, 8);
		this.shape = sample(shapes);
	} else if(arguments.length == 1) {
		this.main_color = main_color.main_color;
		this.secondary_color = main_color.secondary_color;
		this.shape = main_color.shape;
	} else {
		this.main_color = main_color;
		this.secondary_color = secondary_color;
		this.shape = shape;
	}

	this.seed = this.getRandomSeed();

}

PlayerSkin.prototype.drawShape = function(myColor) {

	playercanvas.fillStyle = this.secondary_color;
	playercanvas.strokeStyle = this.secondary_color;
	playercanvas.beginPath();

	switch(this.shape) {
		case "topcircle":
			playercanvas.arc(0, -cellSizePart(0.15), cellSizePart(0.1), 0, Math.PI*2);
			playercanvas.fill();
			playercanvas.beginPath();

			var aux1 = this.calcIntersectionWithPlayer(0, -cellSizePart(0.2), 1, 0, EAST);
			var aux2 = this.calcIntersectionWithPlayer(0, -cellSizePart(0.2), 1, 0, WEST);
			var aux3 = this.calcIntersectionWithPlayer(0, -cellSizePart(0.1), 1, 0, EAST);
			var aux4 = this.calcIntersectionWithPlayer(0, -cellSizePart(0.1), 1, 0, WEST);

			playercanvas.moveTo(aux1.x, aux1.y);
			playercanvas.lineTo(aux2.x, aux2.y);
			playercanvas.lineTo(aux4.x, aux4.y);
			playercanvas.lineTo(aux3.x, aux3.y);
			playercanvas.lineTo(aux1.x, aux1.y);
			playercanvas.fill();

			playercanvas.beginPath();
			playercanvas.fillStyle = myColor;
			playercanvas.arc(0, -cellSizePart(0.15), cellSizePart(0.03), 0, Math.PI*2);
			playercanvas.fill();

		break;
		case "crossline":
			playercanvas.moveTo(-cellSizePart(0.05), cellSizePart(0.3));
			playercanvas.lineTo(-cellSizePart(0.05), -cellSizePart(0.06));

			var aux1 = this.calcIntersectionWithPlayer(-cellSizePart(0.05), -cellSizePart(0.06), -4, -1, WEST);
			var aux2 = this.calcIntersectionWithPlayer(-cellSizePart(0.05), -cellSizePart(0.16), -4, -1, WEST);

			playercanvas.lineTo(aux1.x, aux1.y);
			playercanvas.lineTo(aux2.x, aux2.y);
			playercanvas.lineTo(-cellSizePart(0.05), -cellSizePart(0.16));
			playercanvas.lineTo(cellSizePart(0.05), -cellSizePart(0.16));

			aux1 = this.calcIntersectionWithPlayer(cellSizePart(0.05), -cellSizePart(0.06), 4, -1, EAST);
			aux2 = this.calcIntersectionWithPlayer(cellSizePart(0.05), -cellSizePart(0.16), 4, -1, EAST);

			playercanvas.lineTo(aux2.x, aux2.y);
			playercanvas.lineTo(aux1.x, aux1.y);
			playercanvas.lineTo(cellSizePart(0.05), -cellSizePart(0.06));
			playercanvas.lineTo(cellSizePart(0.05), cellSizePart(0.3));
			playercanvas.lineTo(-cellSizePart(0.05), cellSizePart(0.3));
			playercanvas.fill();
		break;
		case "concentric":
			playercanvas.arc(0, 0, cellSizePart(0.3), 0, Math.PI*2);
			playercanvas.fill();
			playercanvas.beginPath();
			playercanvas.arc(0, 0, cellSizePart(0.25), 0, Math.PI*2);
			playercanvas.fillStyle = myColor;
			playercanvas.fill();
			playercanvas.beginPath();
			playercanvas.arc(0, 0, cellSizePart(0.2), 0, Math.PI*2);
			playercanvas.fillStyle = this.secondary_color;
			playercanvas.fill();
			playercanvas.beginPath();
			playercanvas.arc(0, 0, cellSizePart(0.15), 0, Math.PI*2);
			playercanvas.fillStyle = myColor;
			playercanvas.fill();
			playercanvas.beginPath();
			playercanvas.arc(0, 0, cellSizePart(0.1), 0, Math.PI*2);
			playercanvas.fillStyle = this.secondary_color;
			playercanvas.fill();
			playercanvas.beginPath();
			playercanvas.arc(0, 0, cellSizePart(0.05), 0, Math.PI*2);
			playercanvas.fillStyle = myColor;
			playercanvas.fill();
			playercanvas.beginPath();
		break;
		case "triforce":
			var r = cellSizePart(0.3);
			var side = Math.sqrt(3*Math.pow(r,2)*Math.sin(Math.PI*2/3)/Math.sin(Math.PI/3));
			var high = side*Math.sqrt(3)/2;
			var vertex = new Position(0, -r);
			var inv_vertex = new Position(0, high - r);
			var base_left = this.calcIntersectionWithPlayer(inv_vertex.x, inv_vertex.y, 1, 0, WEST);
			var base_right = this.calcIntersectionWithPlayer(inv_vertex.x, inv_vertex.y, 1, 0, EAST);
			var inv_base_left = new Position((base_left.x - vertex.x)/2, (base_left.y + vertex.y)/2);
			var inv_base_right = new Position((base_right.x - vertex.x)/2, (base_right.y + vertex.y)/2);

			playercanvas.moveTo(vertex.x, vertex.y);
			playercanvas.lineTo(base_left.x, base_left.y);
			playercanvas.lineTo(base_right.x, base_right.y);
			playercanvas.lineTo(vertex.x, vertex.y);
			playercanvas.fill();

			playercanvas.beginPath();
			playercanvas.fillStyle = myColor;
			playercanvas.moveTo(inv_vertex.x, inv_vertex.y);
			playercanvas.lineTo(inv_base_left.x, inv_base_left.y);
			playercanvas.lineTo(inv_base_right.x, inv_base_right.y);
			playercanvas.lineTo(inv_vertex.x, inv_vertex.y);
			playercanvas.fill();
			break;
		case "nazi":
			var pS = new Position(0, -cellSizePart(0.02));
			var pN = new Position(0, -cellSizePart(0.28));
			var inner_side = cellSizePart(0.13);
			var side = cellSizePart(0.13)*Math.sqrt(2);
			var square_side = side/12*Math.sqrt(2);

			playercanvas.moveTo(pS.x, pS.y);
			playercanvas.lineTo(pS.x+4*square_side, pS.y-4*square_side);
			playercanvas.lineTo(pS.x+square_side, pS.y-7*square_side);
			playercanvas.lineTo(pS.x+2*square_side, pS.y-8*square_side);
			playercanvas.lineTo(pS.x+5*square_side, pS.y-5*square_side);
			playercanvas.lineTo(pS.x+6*square_side, pS.y-6*square_side);
			playercanvas.lineTo(pS.x+2*square_side, pS.y-10*square_side);
			playercanvas.lineTo(pS.x-1*square_side, pS.y-7*square_side);
			playercanvas.lineTo(pS.x-2*square_side, pS.y-8*square_side);
			playercanvas.lineTo(pS.x+1*square_side, pS.y-11*square_side);
			playercanvas.lineTo(pS.x, pS.y-12*square_side);
			playercanvas.lineTo(pS.x-4*square_side, pS.y-8*square_side);
			playercanvas.lineTo(pS.x-1*square_side, pS.y-5*square_side);
			playercanvas.lineTo(pS.x-2*square_side, pS.y-4*square_side);
			playercanvas.lineTo(pS.x-5*square_side, pS.y-7*square_side);
			playercanvas.lineTo(pS.x-6*square_side, pS.y-6*square_side);
			playercanvas.lineTo(pS.x-2*square_side, pS.y-2*square_side);
			playercanvas.lineTo(pS.x+1*square_side, pS.y-5*square_side);
			playercanvas.lineTo(pS.x+2*square_side, pS.y-4*square_side);
			playercanvas.lineTo(pS.x-1*square_side, pS.y-1*square_side);
			playercanvas.lineTo(pS.x, pS.y);
			playercanvas.fill();
			break;
		case "horns":
			playercanvas.moveTo(-cellSizePart(0.17), cellSizePart(0.03));
			playercanvas.quadraticCurveTo(-cellSizePart(0.2), -cellSizePart(0.1), -cellSizePart(0.1), -cellSizePart(0.2));
			playercanvas.quadraticCurveTo(-cellSizePart(0.15), -cellSizePart(0.1), -cellSizePart(0.03), 0);
			playercanvas.lineTo(-cellSizePart(0.17), cellSizePart(0.03));
			playercanvas.fill();

			playercanvas.beginPath();
			playercanvas.moveTo(cellSizePart(0.17), cellSizePart(0.03));
			playercanvas.quadraticCurveTo(cellSizePart(0.2), -cellSizePart(0.1), cellSizePart(0.1), -cellSizePart(0.2));
			playercanvas.quadraticCurveTo(cellSizePart(0.15), -cellSizePart(0.1), cellSizePart(0.03), 0);
			playercanvas.lineTo(cellSizePart(0.17), cellSizePart(0.03));
			playercanvas.fill();
			break;
		default:
	}

}

PlayerSkin.prototype.calcIntersectionWithPlayer = function(x1, y1, v1, v2, dir) {

	y1 = -y1;
	if(v2 != 0) v2 = -v2;

	var A, B, C, a, b, c, x, y, m = v2/v1, r = cellSizePart(0.3);
	A = m;
	B = -1;
	C = -m*x1 + y1;
	//console.log("A = " + A + "; B = " + B + "; C = " + C + "; x1 = " + x1 + "; y1 = " + y1);

	a = Math.pow(B,2) + Math.pow(A,2);
	c = Math.pow(C,2) - Math.pow(r,2)*Math.pow(A,2);
	b = 2*C*B;

	if(A != 0 && B != 0) {
		//console.log("a = " + a + "; b = " + b + "; c = " + c);
		y = (-b + Math.sqrt(Math.pow(b,2) - 4*a*c)) / (2*a);
		x = (-C - B*y) / A;
		
		if((dir == NORTH && y > 0) || (dir == SOUTH && y < 0) || (dir == EAST && x < 0) || (dir == WEST && x > 0)) {
			y = (-b - Math.sqrt(Math.pow(b,2) - 4*a*c)) / (2*a);
			x = (-C - B*y) / A;
		}
	} else if (B != 0) {
		y = -C/B;
		x = Math.sqrt(Math.pow(r,2)-Math.pow(y,2));
		if((dir == EAST && x < 0) || (dir == WEST && x > 0)) {
			x = -Math.sqrt(Math.pow(r,2)-Math.pow(y,2));
		}
	} else if (A != 0) {
		x = -C/A;
		y = Math.sqrt(Math.pow(r,2)-Math.pow(x,2));
		if((dir == NORTH && y > 0) || (dir == SOUTH && y < 0)) {
			y = -Math.sqrt(Math.pow(r,2)-Math.pow(x,2));
		}
	} else {
		return new Position(0,0);
	}
	//console.log(x + ", " + y);

	return new Position(x, -y);

}

PlayerSkin.prototype.joinTwoPointsWithArc = function(p1, p2) {

	var starting_angle = this.calculateAngle(p1, new Position(cellSizePart(0.3), 0));
	var ending_angle = this.calculateAngle(p2, new Position(cellSizePart(0.3), 0));
	if(p1.y > 0) starting_angle = (Math.PI*2)-starting_angle;
	else if(p1.y == 0) starting_angle = Math.PI;
	if(p2.y > 0) starting_angle = (Math.PI*2)-starting_angle;
	else if(p2.y == 0) ending_angle = Math.PI;

	playercanvas.arc(0, 0, cellSizePart(0.3), starting_angle, ending_angle);

}

PlayerSkin.prototype.calculateAngle = function(p1, p2) {

	var a = Math.sqrt(Math.pow(p1.x,2) + Math.pow(p1.y,2));
	var b = Math.sqrt(Math.pow(p2.x,2) + Math.pow(p2.y,2));
	var c = Math.sqrt(Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y,2));
	var p = (a+b+c)/2;
	var S = Math.sqrt(p*(p-a)*(p-b)*(p-c));
	return Math.asin((2*S)/(b*c));

}

PlayerSkin.prototype.getRandomSeed = function() {
	return String.fromCharCode(randomNum(33,126)) + String.fromCharCode(randomNum(33,126)) + String.fromCharCode(randomNum(33,126)) + String.fromCharCode(randomNum(33,126));
}