function Castle(size, levels, numberOfTeams, playersForTeam, playerlist, time, itemValue, weaponValue, numberOfGuardians, me, seed) {

	if(size != "onlyrandom") {

		this.onlyrandom = false;
		this.random = false;

		var alpha = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
		for(var i = 0; i < 10; i++) this.afterseed += alpha[randomNum(0, alpha.length-1)];

		if(seed != undefined && seed != null && typeof(seed) == "string" && seed != "") {
			this.seed = seed;
			Math.seedrandom(this.seed);
		} else {
			this.seed = "";
			this.afterseed = "";
			for(var i = 0; i < 10; i++) this.seed += alpha[randomNum(0, alpha.length-1)];
			Math.seedrandom(this.seed);
		}

	}

	if(size == "random" || size == "onlyrandom") {

		this.random = true;
		if(size == "onlyrandom") this.onlyrandom = true;

		var aux = Math.random();
		if(aux < 0.75) size = randomNum(10,14);
		else if(aux < 0.95) size = randomNum(10, 20);
		else if(aux < 0.99) size = randomNum(10,30);
		else size = randomNum(10,40);

		aux = Math.random();
		if(aux < 0.75 || size >= 30) levels = randomNum(4,8);
		else if(aux < 0.95 || size >= 20) levels = randomNum(2, 10);
		else if(aux < 0.99) levels = randomNum(1, 15);
		else levels = randomNum(1, 20);

		if(aux < 0.75 || size*levels <= 15) numberOfTeams = randomNum(3,4);
		else if(aux < 0.95 || size*levels <= 20) numberOfTeams = randomNum(2,5);
		else numberOfTeams = randomNum(2,8);

		if(aux < 0.75  || size*levels <= 15) playersForTeam = randomNum(1,2);
		else if(aux < 0.95 || size*levels <= 20) playersForTeam = randomNum(1,4);
		else if(aux < 0.99) playersForTeam = randomNum(1,6);
		else playersForTeam = randomNum(1,8);

		me = null;
		drawcastle = true;

		if(aux < 0.75  || size*levels <= 15) time = randomNum(120, 300);
		else if(aux < 0.95 || size*levels <= 20) time = randomNum(120, 420);
		else if(aux < 0.99) time = randomNum(120, 560);
		else time = randomNum(120, 720);

		if(aux < 0.75) itemValue = randomNum(Math.floor(size*levels*0.2),Math.floor(size*levels*0.4));
		else if(aux < 0.95 || size*levels <= 20) itemValue = randomNum(Math.floor(size*levels*0.1),Math.floor(size*levels*0.6));
		else if(aux < 0.99) itemValue = randomNum(1, Math.floor(size*levels*0.7));
		else itemValue = randomNum(1, Math.floor(size*levels*0.8));

		if(aux < 0.75) weaponValue = randomNum(Math.floor(size*levels*0.3),Math.floor(size*levels*0.4));
		else if(aux < 0.95 || size*levels <= 20) weaponValue = randomNum(Math.floor(size*levels*0.2),Math.floor(size*levels*0.6));
		else if(aux < 0.99) weaponValue = randomNum(randomNum(Math.floor(size*levels*0.1), Math.floor(size*levels*0.7)));
		else weaponValue = randomNum(1, Math.floor(size*levels*0.8));

		if(aux < 0.75  || size*levels <= 15) numberOfGuardians = randomNum(4, 6);
		else if(aux < 0.95 || size*levels <= 20) numberOfGuardians = randomNum(3, 8);
		else if(aux < 0.99) playersForTeam = randomNum(2, 10);
		else playersForTeam = randomNum(1, 15);
	}

	this.botsActions = [];
	cellSize = Math.floor(canvasize/size) - 1;
	$("#tutorial_square").css("height", cellSize + "px");
	$("#tutorial_square").css("width", cellSize + "px");
	this.me = null;

	this.height = size;
	this.width = size;

	this.cellSize = cellSize; // this is the value in pixels of a cell
	this.rooms = [];
	this.players = [];
	this.me = me || null;
	this.numberOfTeams = numberOfTeams;
	this.playersForTeam = playersForTeam;
	if(typeof(playerlist) == "object") this.playerlist = playerlist;
	else { // Remember to set the bases in the characters TODO
		this.playerlist = [];
		var human = false;
		for(var i = 0; i < this.numberOfTeams; i++) {
			this.playerlist[i] = [];
			for(var j = 0; j < this.playersForTeam; j++) {
				if(!human) {
					this.playerlist[i].push("Ninja " + generarNombres(1, 4, 9)[0]);
					human = true;
				}
				else this.playerlist[i].push("Bot " + generarNombres(1, 4, 9)[0]);
			}
		}
	}

	if(this.me == null && !this.random) this.me = this.playerlist[0][0];

	this.time = time;
	this.ended = false;
	this.numberOfGuardians = numberOfGuardians || 0;

	if(!this.onlyrandom) {

		do {
			this.itemValue = itemValue;
			this.weaponValue = weaponValue;
			numberOfRooms = 0;
			this.rooms = [];
			for(var i = 0; i < levels; i++) {
				this.rooms[i] = new Room(this.height, this.width, this.cellSize); 
			}
			this.connectRooms();
			this.players = [];
			playeridcount = 0;
			this.presentRoom = null;
			keys = ["87,83,68,65,69,82", "38,40,39,37,191,222", "85,74,75,72,73,79", "70,86,66,67,78,77"];
			var error = this.build();
		} while(error);

		Math.seedrandom(this.afterseed);

	} else {
		this.itemValue = itemValue;
		this.weaponValue = weaponValue;
		this.numberOfRooms = levels;
	}

}

Castle.prototype.getPlayer = function(player_id) {
	for(var i in this.players) {
		if(this.players[i].id == player_id) return this.players[i];
	}
	return null;
}

Castle.prototype.changePointOfView = function(newMe) {
	for(var i in this.players) {
		if(this.players[i].name == newMe) {
			this.me = this.players[i];
			this.presentRoom = this.rooms[this.me.position.z];
		}
	}
}

Castle.prototype.start = function() {
	$("#music")[0].play();	
	this.draw();
	if(im_owner) this.botsAct();

	var myCastle = this;
	this.timeCounter = setInterval(function() {
		myCastle.time--;
		if(myCastle.time <= 0) myCastle.endGame();
	}, 1000);
	
	var fps = 1000/60;
	this.refresh_game = setInterval(function() {
		myCastle.drawElements();
	}, fps);
}

Castle.prototype.addElementToRandom = function(typeId) {
	do {
		var pos = new Position(randomNum(1,this.width-2), randomNum(1,this.height-2), sample(this.rooms).id);
		var err = this.addElement(pos, typeId);
	} while(err);
}

Castle.prototype.addElement = function(position, typeId) {
	var cell = this.rooms[position.z].getCell(position);
	if(cell.element != null) return true;
	for(var i = 0; i < 4; i++) {
		if(cell.getCell(i) != null) {
			if(cell.getCell(i).element != null) return true;
			for(var j = 0; j < 4; j++) {
				if(j != i && j != oppositeDirection(i) && cell.getCell(i).getCell(j) != null && cell.getCell(i).getCell(j).element != null) return true;
			}
		}
	}
	this.rooms[position.z].addElement(position, typeId);
	return false;
}

Castle.prototype.removeElement = function(position) {
	this.rooms[position.z].removeElement(position);
}

Castle.prototype.setInitialPoints = function() { // the game starts always in room 0 and every room will be connected
	var initialRoom;
	var initialDirection;
	do {
		initialRoom = this.rooms[randomNum(0, this.rooms.length-1)];
		initialDirection = initialRoom.getPossibleEntrance();
	} while (initialDirection == null);

	if(initialDirection == WEST) {
		this.start = new Position(0, randomNum(0, this.width-1), initialRoom.id);
	} else if(initialDirection == NORTH) {
		this.start = new Position(randomNum(0, this.width-1), 0, initialRoom.id);
	} else if(initialDirection == EAST) {
		this.start = new Position(this.width-1, randomNum(0, this.width-1), initialRoom.id);
	} else {
		this.start = new Position(randomNum(0, this.width-1), this.width-1, initialRoom.id);
	}

	do {
		this.end = new Position(randomNum(2, this.width-3), randomNum(2, this.width-3), randomNum(0, this.rooms.length-1));
	} while(this.start.differences(this.end) < ((initialDirection == WEST || initialDirection == EAST) ? this.width : this.height)*0.75);	

	this.addElement(this.start, "start");
	this.addElement(this.end, "end");
}

Castle.prototype.connectRooms = function() {

	var roomsPlaced = [];
	var possibilities = [];

	refreshPossibilities(this.rooms[0]);

	for(var i = 1; i < this.rooms.length; i++) {
		var chosenRoom = possibilities[randomNum(0,possibilities.length-1)];
		chosenRoom.room.connect(this.rooms[i], chosenRoom.position);
		this.refreshConnections(this.rooms[i]);
		refreshPossibilities(this.rooms[i]);
	}

	function Pair(room, position) {
		this.room = room,
		this.position = position;

		this.equals = function(pair) {
			return this.room == pair.room && this.position == pair.position;	
		}

		this.toString = function() {
			return "[" + this.room.id + ", " + this.position + "]";
		}
	}

	function refreshPossibilities(room) {
		for(var i = 0; i < 6; i++) {
			if(room.getRoom(i) == null) possibilities.push(new Pair(room, i));
			else {
				removePossibility(new Pair(room.getRoom(i), oppositeDirection(i)));	
			} 
		}
	}

	function removePossibility(pair) {
		for(var i = 0; i < possibilities.length; i++) {
			if(pair.equals(possibilities[i])) {
				possibilities.splice(i, 1);
				return;
			}
		}
	}
}

Castle.prototype.refreshConnections = function(initial) {
	var myCastle = this;
	return refreshConnectionsAux([], [initial]);

	function refreshConnectionsAux(directions, placedOnes) {
		var present = placedOnes[placedOnes.length-1];
		var initial = placedOnes[0];
		var connection = myCastle.checkConnection(directions);
		if(connection != null) {
			present.connect(initial, connection);
		}

		for(var i = 0; i < 6; i++) {
			if(present.getRoom(i) != null && !isInArray(present.getRoom(i), placedOnes)) {
				var newDirections = directions.slice();
				newDirections.push(i);
				var newPlacedOnes = placedOnes.slice();
				newPlacedOnes.push(present.getRoom(i));
				placedOnes = refreshConnectionsAux(newDirections, newPlacedOnes);
			}
		}

		return placedOnes;

	}	
}

Castle.prototype.checkConnection = function(directions) {
	if(directions.length < 3) return null;
	var NorthSouthStack = [], EastWestStack = [], UpDownStack = [];
	for(var i in directions) {
		switch(directions[i]) {
			case NORTH:
				if(NorthSouthStack.length == 0 || NorthSouthStack[0] == NORTH) NorthSouthStack.push(NORTH);
				else NorthSouthStack.splice(0, 1);
			break;
			case SOUTH:
				if(NorthSouthStack.length == 0 || NorthSouthStack[0] == SOUTH) NorthSouthStack.push(SOUTH);
				else NorthSouthStack.splice(0, 1);
			break;
			case EAST:
				if(EastWestStack.length == 0 || EastWestStack[0] == EAST) EastWestStack.push(EAST);
				else EastWestStack.splice(0, 1);
			break;
			case WEST:
				if(EastWestStack.length == 0 || EastWestStack[0] == WEST) EastWestStack.push(WEST);
				else EastWestStack.splice(0, 1);
			break;
			case UP:
				if(UpDownStack.length == 0 || UpDownStack[0] == UP) UpDownStack.push(UP);
				else UpDownStack.splice(0, 1);
			break;
			case DOWN:
				if(UpDownStack.length == 0 || UpDownStack[0] == DOWN) UpDownStack.push(DOWN);
				else UpDownStack.splice(0, 1);
			break;
			default:
		}
	}

	if(NorthSouthStack.length == 1 && EastWestStack.length == 0 && UpDownStack.length == 0) return oppositeDirection(NorthSouthStack[0]);
	if(NorthSouthStack.length == 0 && EastWestStack.length == 1 && UpDownStack.length == 0) return oppositeDirection(EastWestStack[0]);
	if(NorthSouthStack.length == 0 && EastWestStack.length == 0 && UpDownStack.length == 1) return oppositeDirection(UpDownStack[0]);
}

Castle.prototype.toStr = function() {
	var res = "";
	for(var i in this.rooms) {
		res += this.rooms[i].toStr() + "\n";
	}
	return res;
}

Castle.prototype.addBotToRandom = function(name, team, skin, onDead, IA, initialWeapon, initialValuableItem, botDelay) {
	var room, cell;
	do {
		room = sample(this.rooms);
		cell = sample(sample(room.cells));
	} while(cell.element != null || cell.characterOverCell != null);

	var player = new Player(name, new Position(0, 0, 0), team, skin, null, onDead, this, IA, initialWeapon, initialValuableItem, botDelay);
	player.position.x = cell.position.x;
	player.position.y = cell.position.y;
	player.position.z = cell.position.z;
	this.players.push(player);

	room.addPlayer(this.players[this.players.length-1]);
}

Castle.prototype.addPlayer = function(name, position, team, skin, keyset, onDead) {
	var newPlayer = new Player(name, new Position(0, 0, 0), team, skin, keyset, onDead, this);
	newPlayer.position.x = position.x;
	newPlayer.position.y = position.y;
	newPlayer.position.z = position.z;

	this.players.push(newPlayer);
	this.rooms[position.z].addPlayer(this.players[this.players.length-1]);
}

Castle.prototype.addBot = function(name, position, team, skin, onDead, IA, initialWeapon, initialValuableItem, botDelay)  {
	var newPlayer = new Player(name, new Position(0, 0, 0), team, skin, null, onDead, this, IA, initialWeapon, initialValuableItem, botDelay);
	newPlayer.position.x = position.x;
	newPlayer.position.y = position.y;
	newPlayer.position.z = position.z;

	this.players.push(newPlayer);
	this.rooms[position.z].addPlayer(this.players[this.players.length-1]);
}

Castle.prototype.movePlayer = function(player, dir, randomNum) {

	if(randomNum == null || randomNum == undefined) randomNum = Math.random();

	if(dir == -1 || ((player.dead || this.ended) && player.id != 0) || (tutorial_mode && tutorial_talking.length > 0)) return;

	if(((player.dead || this.ended) && player.id == this.me.id)) {
		if(this.presentRoom.getRoom(dir) != null) {
			this.presentRoom = this.presentRoom.getRoom(dir);
			this.draw();
		}
		return;
	}

	var room = this.rooms[player.position.z];
	var playerCell = room.getCell(player.position);

	if(dir == 4) {
		if(player.weapon != null) {
			if(playerCell.element != null && playerCell.element.type.isWeapon != undefined) {
				var aux = playerCell.removeElement().type;
				if(player.weapon.name != "Bomb") playerCell.setElement(player.weapon.name);
				else playerCell.putBomb();
				player.weapon = aux;
			} else if(playerCell.element == null) {
				if(player.weapon.name != "Bomb") playerCell.setElement(player.weapon.name);
				else playerCell.putBomb();
				player.weapon = null;
			}
		}
		if(player.id == 0 && tutorial_mode) checkTutorial();
		return;
	} else if(dir == 5) {
		if(player.valuableItem != null) {
			if(playerCell.element != null && playerCell.element.type.isValuableItem != undefined) {
				var aux = playerCell.removeElement().type;
				playerCell.setElement(player.valuableItem.name);
				player.valuableItem = aux;
			}
			else if(playerCell.element == undefined) {
				playerCell.setElement(player.valuableItem.name);
				player.valuableItem = null;
			}
		}
		if(player.id == 0 && tutorial_mode) checkTutorial();
		return;
	}

	if(playerCell.getCell(dir) != null && playerCell.hasPath(dir) && playerCell.getCell(dir).characterOverCell != null && !player.isSameTeam(playerCell.getCell(dir).characterOverCell)) {
		if(dir <= 3) player.lookDirection = dir;
		player.attack(playerCell.getCell(dir).characterOverCell, randomNum);
		if(player.dead) return;
	}

	if(playerCell.hasPath(dir)) {
		playerCell.characterOverCell = null;
		switch(dir) {
			case NORTH:
				if(player.position.y - 1 >= 0) {
					player.position.y--;
				}
			break;
			case EAST:
				if(player.position.x - 1 < this.width) {
					player.position.x++;
				} 
			break;
			case SOUTH:
				if(player.position.y - 1 < this.height) {
					player.position.y++;
				} 
			break;
			case WEST:
				if(player.position.x - 1 >= 0) {
					player.position.x--;					
				} 
			break;
			default:
		}
	}

	if(dir <= 3) player.lookDirection = dir;
	player.step();
	playerCell = room.getCell(player.position);
	getCell(player.position).characterOverCell = player;

	if(playerCell.element != null) {
		if(playerCell.element.type.name.indexOf("passage") > -1 && (playerCell.element.type.direction > 3 || (player.lookDirection == playerCell.element.type.direction))) {
			player.stopFocusing();
			var nextRoom = room.getRoom(playerCell.element.type.direction);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			if(player.id == this.me.id) console.log(nextRoom.id);
			player.position = playerCell.element.type.getExit(playerCell.position, this.height, this.width, nextRoom);
			var newPlayerCell = getCell(player.position);
			room.removePlayer(player);

			if(newPlayerCell != null && newPlayerCell.characterOverCell != null && player.team != newPlayerCell.characterOverCell.team) {
				player.attack(newPlayerCell.characterOverCell, randomNum);
				if(!player.dead) {
					nextRoom.addPlayer(player);
					playerCell.characterOverCell = null;
					newPlayerCell.characterOverCell = player;
				}
				if(player.dead) return;
			}
			else {
				nextRoom.addPlayer(player);
				playerCell.characterOverCell = null;
				newPlayerCell.characterOverCell = player;
			}
			if(player.id == this.me.id) {
				this.presentRoom = nextRoom;
				this.draw();
			}
		}
		else if(playerCell.element.type.isValuableItem != undefined && player.valuableItem == null) {
			player.valuableItem = playerCell.removeElement().type;
			playerCell.element = null;
		}
		else if(playerCell.element.type.isWeapon != undefined && player.weapon == null) {
			player.weapon = playerCell.removeElement().type;
			playerCell.element = null;
		}
		else if(playerCell.element.type.name == "Base" + player.team) {
			if(player.valuableItem != null) {
				this.teamModifiesMoney(player.team, player.valuableItem.value);
				player.moneyCollected += player.valuableItem.value;
				playerCell.element.type.items.push(player.valuableItem);
				player.valuableItem = null;
			}
		} else if(playerCell.element.type.name.indexOf("Base") > -1 && playerCell.element.type.name != "Baseball_Bat" && player.valuableItem == null && playerCell.element.type.items.length > 0) {
			var enemyId = parseInt(playerCell.element.type.name.replace("Base",""));
			if(playerCell.element.type.vulnerable(this.players, this.playersForTeam)) {
				player.valuableItem = playerCell.element.type.stealItem(this);
				player.steals++;
			}
		}

	}

	if(player.id == 0 && tutorial_mode) checkTutorial();

}
// UP, DOWN, RIGHT, LEFT, WEAPON, ITEM
// wsdaer, "keys"ç´, ujkhio, fvbcnm
var keys = ["87,83,68,65,69,82", "38,40,39,37,191,222", "85,74,75,72,73,79", "70,86,66,67,78,77"];

Castle.prototype.build = function() {

	var empty_rooms = [];

	for(var i = 0; i < this.numberOfTeams; i++) {

		if(empty_rooms.length <= 0) {
			for(var j in this.rooms) {
				empty_rooms.push(this.rooms[j]);
			}
		}

		var skin = new PlayerSkin();
		for(var j = 0; j < this.playersForTeam; j++) {
			if(this.playerlist[i][j].indexOf("Bot ") != 0) this.addPlayer(this.playerlist[i][j], new Position(0,0,0), i, skin, keys[0], "revive_base");
			else this.addBot(this.playerlist[i][j], new Position(0,0,0), i, skin, "revive_base", "defaultBot", null, null, randomNum(0,75));

			if(this.playerlist[i][j] == this.me || this.me == null) this.me = this.players[this.players.length-1];
		}
		if(searchElement("Base" + i) != null) elementtypes.splice(searchElement("Base" + i), 1);
		elementtypes.push(new Base(i, this.players[this.playersForTeam*i]));
		var pos;
		do {
			pos = new Position(randomNum(1,this.width-2), randomNum(1,this.height-2), sample(empty_rooms).id);
			var err = this.addElement(pos, "Base"+i);
		} while(err);

		for(var j = 0; j < empty_rooms.length; j++) {
			if(empty_rooms[j].id == pos.z) {
				empty_rooms.splice(j, 1);
				break;
			}
		}

		for(var j = 0; j < this.playersForTeam; j++) {
			var playerId = this.playersForTeam*i + j;
			this.players[playerId].position.x = pos.x;
			this.players[playerId].position.y = pos.y;
			this.players[playerId].position.z = pos.z;
			this.rooms[pos.z].getCell(new Position(pos.x, pos.y)).characterOverCell = this.players[playerId];
			this.players[playerId].base = pos;
		}
	}

	var guardians_skin = new PlayerSkin();
	for(var i = 0; i < this.numberOfGuardians; i++) {
		this.addBotToRandom("Guardian " + generarNombres(1, 4, 9)[0], 99, guardians_skin, "random_replace", "guardian", null, null, 50);
	}

	// String de tipo Coin|2,Book,Candelabro|1
	if(typeof(this.itemValue) == "string") {
		var items = this.itemValue.split(",");
		for(var i in items) {
			var aux = items[i].split("|");
			if(aux.length == 1) aux[1] = 1;
			for(var j = 0; j < aux[1]; j++) {
				this.addElementToRandom(aux[0]);
			}
		}
	} else {
		var items = [];
		var totalvalue = 0;

		// Añado a una lista los valuableItems
		for(var i in elementtypes){
			if(elementtypes[i].isValuableItem != undefined && elementtypes[i].value <= this.itemValue) {
				items.push(elementtypes[i]);
				totalvalue += elementtypes[i].value;
			}
		}
		// La ordeno de menor a mayor
		items.sort(function(a,b) {
			if(a.value >= b.value) return 1;
			else return -1;
		});

		var maxvalue = items[items.length-1].value;
		// Creo una lista de probabilidades inversas (el objeto mas probable es el mas chiquitin)
		var chances = [];
		for(var i = items.length-1; i >= 0; i--) {
			chances.push(items[i].value/totalvalue);
		}

		do {
			// Compruebo si algun item se ha pasado del rango, y si es asi lo elimino
			if(this.itemValue < maxvalue) {
				for(var i = 0; i < items.length; i++) {
					if(this.itemValue < items[i].value) {
						totalvalue -= items[i].value;
						items.splice(i--, 1);
					}
				}
				// Calculo el nuevo maxvalue. La lista esta ordenada
				maxvalue = items[items.length-1].value;
				// Recalculo probabilidades
				chances = [];
				for(var i = items.length-1; i >= 0; i--) {
					chances.push(items[i].value/totalvalue);
				}
			}

			// Consigo un numero al azar que corresponde a un objeto y lo añado
			var rnd = Math.random(), aux = 0;
			for(var i in chances) {
				aux += chances[i];
				if(rnd < aux) {
					this.addElementToRandom(items[i].name);
					this.itemValue -= items[i].value;
					break;
				}
			}
		} while(this.itemValue > 0 && items.length > 0);
	}

	// String de tipo Coin|2,Book,Candelabro|1
	if(typeof(this.weaponValue) == "string") {
		var weapons = this.weaponValue.split(",");
		for(var i in weapons) {
			var aux = weapons[i].split("|");
			if(aux.length == 1) aux[1] = 1;
			for(var j = 0; j < aux[1]; j++) {
				this.addElementToRandom(aux[0]);
			}
		}
	} else {
		var aux = [];
		var levels = [];

		// Añado a una lista las weapons
		for(var i in elementtypes){
			if(elementtypes[i].isWeapon != undefined && elementtypes[i].level <= this.weaponValue && elementtypes[i].name != "Bomb") {
				aux.push(elementtypes[i]);
				if(levels.indexOf(elementtypes[i].level) == -1) levels.push(elementtypes[i].level); 
			}
		}
		// Creo la lista de armas separadas por niveles
		var weapons = [];
		for(var i in levels) {
			weapons[i] = [];
			for(var j in aux) {
				if(aux[j].level == levels[i]) weapons[i].push(aux[j]);
			}
		}

		do {
			for(var i = 0; i < weapons.length; i++) {
				if(Math.random() < 0.8 || weapons.length == (i+1) || levels[i+1] > this.weaponValue) {
					var chosen = sample(weapons[i]);
					this.addElementToRandom(chosen.name);
					this.weaponValue -= chosen.level;
					break;
				}	
			}
		} while(this.weaponValue > 0);
	}

	//this.setInitialPoints();

	for(var i in this.rooms) {
		for(var j = 0; j < 6; j++) {
			if(this.rooms[i].getRoom(j) != null && this.rooms[i].getRoom(j).id > this.rooms[i].id) {
				var times = 100;
				do {
					var passPosition = this.getPassagePositions(j, this.rooms[i]);
					var err = this.addElement(passPosition[0], "passage" + j);
					if(!err) {
						err = this.addElement(passPosition[1], "passage" + oppositeDirection(j));
						if(err) this.removeElement(passPosition[0]);
					}
					times--;
					if(times <= 0) return true;
				} while(err);
			}
		}
	}

	for(var i in this.rooms) {

		var error = this.rooms[i].build();
		if(error) return true;
	}

	this.presentRoom = this.rooms[this.me.position.z];
	return false;
	/*
		Okay, lets think about the algorithm:
		1: set up the path that player must follow to complete the puzzle (only between rooms), ending it at the target to kill. (Maybe more than one??? Think about it!)
		2: build doors, stairs, buttons, enemies...
		3: build walls to separate the different 'subrooms' in the rooms (according to the step 2)
		4: once every obstacle has been placed in the castle, finish it with 'random' walls according with the laberynth generator algorithm
	*/
}

Castle.prototype.getPassagePositions = function(dir, room) {
	
	var nextRoom = room.getRoom(dir);

	var result = [new Position(0, 0, room.id), new Position(0, 0, nextRoom.id)];

	do {
		switch(dir) {
			case NORTH:
				result[0].x = randomNum(1, this.width-2);
				result[0].y = 0;
				result[1].x = result[0].x
				result[1].y = this.height-1;
				break;
			case SOUTH:
				result[0].x = randomNum(1, this.width-2);
				result[0].y = this.height-1;
				result[1].x = result[0].x
				result[1].y = 0;
				break;
			case EAST:
				result[0].x = this.width-1;
				result[0].y = randomNum(1, this.height-2);
				result[1].x = 0;
				result[1].y = result[0].y;
				break;
			case WEST:
				result[0].x = 0;
				result[0].y = randomNum(1, this.height-2);
				result[1].x = this.width-1;
				result[1].y = result[0].y;
				break;
			default:
				result[0].x = randomNum(1, this.width-2);
				result[0].y = randomNum(1, this.height-2);
				result[1].x = result[0].x;
				result[1].y = result[0].y;
		}
	} while(room.getCell(result[0]).element != null || nextRoom.getCell(result[1]).element != null || 
		room.getCell(result[0]).isRoundedByElement() || nextRoom.getCell(result[1]).isRoundedByElement());

	return result;

};

Castle.prototype.draw = function() {
	$("#castle")[0].width = $("#castle")[0].width;
	$("#player")[0].width = $("#player")[0].width;
	this.presentRoom.draw();
	for(var i in this.players) {
		this.players[i].draw();
	}
	this.drawElements();
}

Castle.prototype.drawElements = function() {
	$("#player")[0].width = $("#player")[0].width;
	this.presentRoom.drawElements();
	for(var i in this.players) {
		if(this.players[i].position.z == this.presentRoom.id && !this.players[i].dead) this.players[i].draw();
	}
	this.refreshPlayerList();
}

Castle.prototype.refreshPlayerList = function() {
	var res = "";
	if(this.ended) {
		res = "GAME OVER";
		var maxMoney = 0;
		var teamWinner = [];

		for(var i in this.players) {
			var player = this.players[i];
			if(player.money == maxMoney && teamWinner.indexOf(player.team) == -1) {
				teamWinner.push(player.team);
			}
			else if(player.money > maxMoney) {
				maxMoney = player.money;
				teamWinner = [player.team];
			}
		}

		res += "<br>Winners:<br><table>";

		for(var i in this.players) {
			var player = this.players[i];
			if(teamWinner.indexOf(player.team) > -1) res += this.players[i].toStats();
		}

		res += "</table>Losers:<br><table>";

		for(var i in this.players) {
			var player = this.players[i];
			if(teamWinner.indexOf(player.team) == -1) res += this.players[i].toStats();
		}

	} else {
		res += "Time left: " + toHMStime(this.time) + "<br><table><tr><td>Player</td><td>DEAD</td><td><strong>Money</strong></td><td><font color='blue'>Kills</font></td><td><font color='red'>Deads</font></td><td><font color='#cccc00'>Money Collected</font></td><td><font color='green'>Steals</font></td><td><font color='purple'>Luck</font></td></tr>";
		for(var i in this.players) {
			res += this.players[i].toStats();
		}
	}
	$("#stats").html(res + "</table>");
}

Castle.prototype.botsAct = function() {
	for(var i in this.players) {
		if(this.players[i].IA != null && this.players[i].IA != undefined) this.players[i].startAction();
	}
}

/*Castle.prototype.playerOfTeamDead = function(team) {
	for(var i in this.players) {
		if(this.players[i].dead && this.players[i].team == team) return true;
	}
	return false;
}*/

Castle.prototype.endGame = function() {

	clearInterval(this.timeCounter);

	this.players.sort(function(a, b) {
		if(a.money >= b.money) return -1;
		else return 1;
	});

	this.ended = true;

	this.drawElements();

	clearInterval(this.refresh_game);

	if(!one_player) socket.emit("kill_game"); // TODO

}

Castle.prototype.teamModifiesMoney = function(team_id, quantity) {

	for(var i in this.players) {
		if(this.players[i].team == team_id) this.players[i].money += quantity;
		if(this.players[i].money < 0) this.players[i].money = 0;
	}

}

Castle.prototype.getCode = function() {
	var res = "";
	for(var i = 0; i < this.rooms.length; i++) {
		res += this.rooms[i].getCode();
		if(i < this.rooms.length-1) res += "|";
	}
	return res;
}

function toHMStime(time) {

	if(time < 60) return time;

	var res = "";
	var mins = Math.floor(time/60);
	var secs = time%60;
	var hours = Math.floor(mins/60);
	mins %= 60;

	if(hours > 9) res += hours + ":";
	else if(hours > 0) res += "0" + hours + ":";

	if(mins > 9) res += mins + ":";
	else if(mins > 0) res += "0" + mins + ":";
	else if(hours > 0) res += "00:";

	if(secs > 9) res += secs;
	else if(secs > 0) res += "0" + secs;
	else res += "00";

	return res;
}