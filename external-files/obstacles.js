var elementtypecount = 0, obstacletypecount = 0;
const SLOW = 0, MEDIUM = 1, FAST = 2; //speeds
const DEADLY = -1; //damage
const STATIC_ONEDIRECTION = 0, STATIC_TWODIRECTIONS = 1, STATIC_FOURDIRECTIONS = 2, STATIC_RANDOM = 3, DYNAMIC_LINEAR = 4, DYNAMIC_INTELLIGENT = 5, DYNAMIC_RANDOM = 6;

var elementtypes = [
	new PassageType(NORTH),
	new PassageType(SOUTH),
	new PassageType(WEST),
	new PassageType(EAST),
	new PassageType(UP),
	new PassageType(DOWN),
	new Trigger("key", ["keydoor"]),
	new SpecialCell("end", true),
	new SpecialCell("start", true),
	new SpecialCell("Bomb_Exploding", true, "bomb_fuse1.mp3"),
	new SpecialCell("Bomb_Exploding2", true),
	new SpecialCell("Bomb_Exploding3", true, "bomb_explosion.mp3"),
	new Weapon("Baseball_Bat", 1, "", "punch1.mp3,punch2.mp3"),
	new Weapon("Sword", 1, "Dagger,Sharpened_Dagger,Baseball_Bat,", "sword1.mp3,sword2.wav"),
	new Weapon("Dagger", 1, "Shield,Huge_Shield,Baseball_Bat,", "dagger1.mp3,dagger2.mp3"),
	new Weapon("Shield", 1, "Sword,Long_Sword,Baseball_Bat,", "shield1.mp3"),
	new Weapon("Long_Sword", 2, "Dagger,Sharpened_Dagger,Baseball_Bat,", "sword2.wav,sword3.wav"),
	new Weapon("Sharpened_Dagger", 2, "Shield,Huge_Shield,Baseball_Bat,", "dagger1.mp3,dagger2.mp3"),
	new Weapon("Huge_Shield", 2, "Sword,Long_Sword,Baseball_Bat,", "shield1.mp3"),
	new Weapon("Flaming_Blade", 3, "", "flaming_blade1.mp3,flaming_blade2.mp3"),
	new Weapon("Bomb", 0, ""),
	new ValuableItem("Coin", "coin.png", 1),
	new ValuableItem("Teddy", "peluche.png", 10),
	new ValuableItem("Chip", "chip.png", 4),
	new ValuableItem("Chicken", "pollo.png", 2),
	new ValuableItem("Chandelier", "candelabro.png", 5),
	new ValuableItem("Book", "book.png", 3)
];

var obstacletypes = [
	new Door("keydoor"),
	new Enemy("Ninja Warden", "ninjawarden.png", DYNAMIC_RANDOM, 3, ["Poisoned Shuriken"], [])
];

function ObstacleType(name) {
	this.id = obstacletypecount;
	this.name = name;
	obstacletypecount++;
}

function ElementType(name) {
	this.id = elementtypecount;
	this.name = name;
	elementtypecount++;
}

function SpecialCell(name, walkable, sounds) {
	this.name = name;
	this.walkable = walkable;
	if(sounds != undefined && sounds != null) {
		var splitted = sounds.split();
		this.sounds = [];
		for(var i in splitted) {
			this.sounds.push(new Audio("https://dl.dropboxusercontent.com/u/5466518/PerfectOperation/sounds/" + splitted[i]));
		}
	}

}

SpecialCell.prototype.draw = function(element) {
	if(element != null) drawImage(element.position, this.name);
}

function Base(id, player) {
	this.name = "Base" + id;
	this.items = [];
	this.player = player;
	this.id = id;
	this.VULNERABLE_CONSTANT = 34; // % of players that must be alive for not being a vulnerable base
}

Base.prototype.vulnerable = function(players, playersForTeam) {
	var aux_proportion = 100;

	for(var i in players) {
		var player = players[i];
		if(player.team == this.id && this.items.length > 0 && player.dead) {
			aux_proportion -= (100/playersForTeam);
		}
		if(aux_proportion < this.VULNERABLE_CONSTANT) {
			return true;
		} 
	}
	return false;
}

Base.prototype.stealItem = function(myCastle) {

	if(this.items.length == 0) return null;

	var mostValuableIds = [];
	var bestValue = 0;

	for(var i in this.items) {
		if(this.items[i].value > bestValue) {
			mostValuable = [i];
			bestValue = this.items[i].value;
		}
		else if(this.items[i].value == bestValue) mostValuable.push(i);
	}

	var stolenId = sample(mostValuable);
	var stolenItem = this.items[stolenId];
	this.items.splice(stolenId, 1);
	myCastle.teamModifiesMoney(this.id, -stolenItem.value);
	return stolenItem;
}

function Weapon(name, level, strongAgainst, sounds) {
	this.name = name;
	this.level = level;
	this.strongAgainst = strongAgainst;
	this.isWeapon = true;
	this.sounds = [];
	if(sounds != undefined && sounds != null) {
		var splitted = sounds.split(",");
		for(var i in splitted) {
			this.sounds.push(new Audio("https://dl.dropboxusercontent.com/u/5466518/PerfectOperation/sounds/" + splitted[i]));
		}
	}
}

Weapon.prototype.chance = function(anotherWeapon) {

	if(this.name != "Bomb" && (anotherWeapon == null || anotherWeapon.name == "Bomb")) return 1;
	if(this.name == "Bomb" && anotherWeapon != null && anotherWeapon.name != "Bomb") return 0;
	else if(this.name == "Bomb") return 0.5;

	var difference = this.level - anotherWeapon.level;
	var mineStronger = this.strongAgainst.indexOf(anotherWeapon.name + ",") > -1;
	var enemiesStronger = anotherWeapon.strongAgainst.indexOf(this.name + ",") > -1;

	if(mineStronger && difference > 0) return 1;
	else if(enemiesStronger && difference < 0) return 0;
	else if(mineStronger && difference == 0) return 0.9;
	else if(enemiesStronger && difference == 0) return 0.1;
	else if(mineStronger && difference == -1) return 0.5;
	else if(enemiesStronger && difference == 1) return 0.5;
	else if(mineStronger && difference == -2) return 0.1;
	else if(enemiesStronger && difference == 2) return 0.9;
	else if(mineStronger && difference < -2) return 0;
	else if(enemiesStronger && difference > 2) return 1;
	else if(difference == 1) return 0.75;
	else if(difference == -1) return 0.25;
	else if(difference == 2) return 0.95;
	else if(difference == -2) return 0.05;
	else if(difference == 0) return 0.5;
	else if(difference > 2) return 1;
	else if(difference < -2) return 0;

}

Weapon.prototype.draw = function(element) {
	if(element != null) drawImage(element.position, this.name);
}

function ValuableItem(name, image, value) {
	this.name = name;
	this.image = image;
	this.value = value;
	this.isValuableItem = true;
}

ValuableItem.prototype.draw = function(element) {
	if(element != null) drawImage(element.position, this.name);
}

ValuableItem.prototype.toString = function() {
	return this.name + "#" + this.value;
}

function Trigger(name, possibleSolvers) {
	this.name = name;
	this.possibleSolvers = possibleSolvers;
}

function PassageType(dir) {
	this.name = "passage" + dir;
	this.direction = dir;
}

PassageType.prototype.getExit = function(position, height, width, targetRoom) {
	switch(this.direction) {
		case NORTH:
			return new Position(position.x, height-1, targetRoom.id);
		case SOUTH:
			return new Position(position.x, 0, targetRoom.id);
		case EAST: 
			return new Position(0, position.y, targetRoom.id);
		case WEST:
			return new Position(width-1, position.y, targetRoom.id);
		case UP:
			return new Position(position.x, position.y, targetRoom.id);
		case DOWN:
			return new Position(position.x, position.y, targetRoom.id);
		default:
			return null;
	}
}

Weapon.prototype = new ElementType(this.name);
Trigger.prototype = new ElementType(this.name);
PassageType.prototype = new ElementType(this.name);

function Enemy(name, img, movementType, timeBetweenMoves, weapons, items) {
	this.name = name;
	this.img = img;
	this.movementType = movementType;
	this.timeBetweenMoves = timeBetweenMoves;
	this.weapons = [];
	for(var i in weapons) {
		this.weapons.push(searchElement(weapons[i]));
	}
	this.items = [];
	for(var i in items) {
		this.items.push(searchElement(items[i]));
	}
}

function Door(name, color) {
	this.name = name;
}

Enemy.prototype = new ObstacleType(this.name);
Door.prototype = new ObstacleType(this.name);

var element_count_id = 0;
function Element(position, typeId) {
	this.position = new Position(position);
	this.setType(typeId);
	this.obstacleId = null;
	this.id = element_count_id++;
}

Element.prototype.toString = function() {
	return this.type.name + " " + this.position;
}

Element.prototype.toString = function() {
	return this.type.name;
}

Element.prototype.draw = function() {
	this.type.draw(this);
}

Element.prototype.setType = function(typeId) {
	if(typeof(typeId) == "string") {
		this.typeId = searchElement(typeId);
		this.type = getElementType(typeId);
	} else if(typeId == undefined) {
		this.type = null;
	} else { // is an object
		this.typeId = searchElement(typeId.name);
		this.type = typeId;
	}
}

function getElementType(typeId) {
	return elementtypes[searchElement(typeId)];
}

function searchElement(typeId) {
	for(var i in elementtypes) {
		if(elementtypes[i].name === typeId) return parseInt(i);
	}
	return null;
}

function searchObstacle(typeId) {
	for(var i in obstacletypes) {
		if(obstacletypes[i].name === typeId) return i;
	}
}

/*

ideas:

alarmas (desactivar)

*/