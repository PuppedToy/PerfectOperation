var playeridcount = 0;

function Player(name, position, team, skin, keyset, onDead, castle, IA, initialWeapon, initialValuableItem, botDelay) {

	this.name = name;
	this.position = position;
	this.team = team;
	this.lookDirection = SOUTH;
	this.movingDirection = [];
	this.moving = null;
	this.id = playeridcount++;
	this.weapon = initialWeapon || null;
	this.items = [];
	this.valuableItem = initialValuableItem || null;
	this.money = 0;
	this.luck = 0;
	this.base = null; // I have to asign a base to a player
	this.skin = skin;
	this.color = this.skin.main_color;
	this.keyset = ((keyset == undefined || keyset == null) ? [] : keyset.split(","));
	this.rightFoot = 0;
	this.nextStep = 0.25;
	this.dead = false;
	this.invincible = false;
	this.kills = 0;
	this.deads = 0;
	this.moneyCollected = 0;
	this.steals = 0;
	this.castle = castle;
	this.onDead = onDead;
	this.IA = IA || null;

	if(this.IA != undefined && this.IA != null) {
		this.chasing = null;
		this.visitedRoomsIA = [];
		this.lastVisitedRoom = null;
		this.focusedCell = null;
		this.nextActions = [];
		this.botDelay = botDelay || 0;
		this.stealingBase = null;
		this.reactionToCharacter = [];
		this.characterMadeReaction = null;
		this.guarding_time = 100;
	}
}

Player.prototype.toString = function() {
	return this.name;
}

Player.prototype.move = function(keycode) {

	for(var i = 0; i < this.keyset.length; i++) {
		if(this.keyset[i] == keycode) return i;
	}

	return -1;

}

Player.prototype.step = function() {
	this.rightFoot += this.nextStep;
	if(this.rightFoot >= 1 || this.rightFoot <= 0) this.nextStep = this.nextStep*(-1);
}

Player.prototype.die = function() {
	if(!this.dead) {
		this.dead = true;
		this.deads++;
		this.nextActions = [];
		this.reactionToCharacter = [];
		this.visitedRoomsIA = [];
		this.stopAction();
		if(this.onDead != "no_drop") this.dropItems();
		getCell(this.position).roomUp.removePlayer(this);
		getCell(this.position).characterOverCell = null;
		if(this.onDead == "revive_base") {
			this.invincible = true;
			this.position = new Position(this.base);
			if(this.id == this.castle.me.id) {
				this.castle.presentRoom = this.castle.rooms[this.position.z];
				this.castle.draw();
			}
			var myPlayer = this;
			setTimeout(function() {
				myPlayer.dead = false;
				myPlayer.castle.rooms[myPlayer.base.z].addPlayer(myPlayer);
				if(myPlayer.id == myPlayer.castle.me.id) {
					myPlayer.castle.presentRoom = myPlayer.castle.rooms[myPlayer.position.z];
					myPlayer.castle.draw();			
				}
				if(myPlayer.IA != null && myPlayer.IA != undefined && im_owner) myPlayer.restartAction();
				setTimeout(function() {
					myPlayer.chasing = null;
					myPlayer.reactionToCharacter = [];
					myPlayer.invincible = false;
				}, 2000);
			}, DEADTIME);
		} else if(this.onDead == "random_replace") {
			var random_cell;
			do {
				random_cell = sample(sample(sample(this.castle.rooms).cells));
			} while (random_cell.element != null);
			this.position.x = random_cell.position.x;
			this.position.y = random_cell.position.y;
			this.position.z = random_cell.position.z;
			var myPlayer = this;
			setTimeout(function() {
				myPlayer.dead = false;
				random_cell.roomUp.addPlayer(myPlayer);
				if(myPlayer.IA != null && myPlayer.IA != undefined && im_owner) myPlayer.restartAction();
			}, DEADTIME);
		}
	}
}

// A weapon lvl 1 has 25% chance to win a lvl 2
// A weapon lvl 1 strong against lvl 2 has 50% chance to win
// A weapon lvl 1 strong against lvl 3 has 10% chance to win
// A weapon lvl 1 strong against lvl 4 cant win
// A weapon lvl 1 has 5% chance to win a lvl 3
// A weapon lvl 1 strong against a weapon lvl 1 has 90% chance to win
// A weapon lvl 2 strong against a weapon lvl 1 has 100% chance to win
Player.prototype.attack = function(enemyPlayer, randomNum) {
	var chance, from;

	if(this.lookDirection == enemyPlayer.lookDirection || (this.invincible && !enemyPlayer.invincible)) {
		chance = 1;
		if(!this.invincible) from = "BEHIND";
		else from = null;
	}
	else if(enemyPlayer.invincible && !this.invincible) {
		chance = 0;
		from = null;
	}
	else {

		if(this.hasNoWeapon() && enemyPlayer.hasNoWeapon()) chance = 0.5;
		else if(this.hasNoWeapon()) chance = 0;
		else if(enemyPlayer.hasNoWeapon()) chance = 1;
		else chance = this.weapon.chance(enemyPlayer.weapon);

		if(this.lookDirection != oppositeDirection(enemyPlayer.lookDirection)) {
			chance += (1-chance)/2;
			from = "THE SIDE";
		} else from = "FRONTWARDS"
	}

	if(this.castle.me.id == this.id) this.fillHitInformation(enemyPlayer, randomNum, chance, true, from, randomNum < chance);
	else if(this.castle.me.id == enemyPlayer.id) enemyPlayer.fillHitInformation(this, randomNum, (1 - chance), false, from, randomNum >= chance);

	if(randomNum < chance) {
		this.luck += (1 - chance);
		enemyPlayer.luck -= (1 - chance);
		this.kill(enemyPlayer);
	}
	else {
		this.luck -= chance;
		enemyPlayer.luck += chance;
		enemyPlayer.kill(this);
	}
}

Player.prototype.fillHitInformation = function(enemyPlayer, randomNum, chance, me_attacked, from, i_won) {

	var color = (i_won ? "blue" : "red");
	var lh_enemy = "<font color='" + color + "'>" + (me_attacked ? "I attacked " : "I was attacked by ") + "</font><strong><font color='" + enemyPlayer.color + "'>" + enemyPlayer.name + "</font></strong><font color='" + color + "'>" + (from == null ? "" : (" from <strong>" + from + "</strong>")) + "</font>";
	var lh_chance = "<font color='" + color + "'>" + "I had a chance of <strong>" + (chance * 100) + "% " + (from == null ? "(INVINCIBLE)" : "") + "</strong> to win and I <strong>" + (i_won ? " WON" : " LOST") + "</strong></font>";
	var my_weapon = this.getWeaponData();
	var foe_weapon = enemyPlayer.getWeaponData();

	$("#lh_enemy").html(lh_enemy);
	$("#lh_m_img").css("background-image", my_weapon.src);
	$("#lh_f_img").css("background-image", foe_weapon.src);
	$("#lh_m_lvl").html("<font color='" + color + "'>" + my_weapon.name +  " LEVEL " + my_weapon.lvl + "</font>");
	$("#lh_f_lvl").html("<font color='" + color + "'>" + foe_weapon.name +  " LEVEL " + foe_weapon.lvl + "</font>");
	$("#lh_chance").html(lh_chance);

}

/*<div id="lasthit_box">
	<div id="lh_enemy"></div>
	<div id="lh_myweapon"><div id="lh_m_img"></div><div id="lh_m_lvl"></div></div><div id="lh_foeweapon"><div id="lh_f_img"></div><div id="lh_f_lvl"></div></div>
	<div id="lh_chance"></div>
</div>*/

Player.prototype.getWeaponData = function() {
	var data;
	if(this.weapon == null || this.weapon.name == "Bomb") {
		data = {
			src: 'url("' + $("#Punch").attr("src") + '")',
			lvl: 0,
			name: "Punch"
		};
	}
	else {
		data = {
			src: 'url("' + $("#" + this.weapon.name).attr("src") + '")',
			lvl: this.weapon.level,
			name: this.weapon.name
		};	
	}
	return data;
}

Player.prototype.kill = function(enemyPlayer) {
	this.kills++;
	this.chasing = null;
	this.reactionToCharacter = [];
	if(this.position.z == this.castle.presentRoom.id || enemyPlayer.position.z == this.castle.presentRoom.id) this.playSoundKill();
	enemyPlayer.die();
}

Player.prototype.startMoving = function() {
	var myPlayer = this;
	this.moving = setInterval(function() {
		if(one_player) myPlayer.castle.movePlayer(myPlayer, myPlayer.movingDirection[0]);
		else socket.emit("move", myPlayer.id, myPlayer.movingDirection[0]);
	}, MOVING_TIME);
}

Player.prototype.stopMoving = function() {
	clearInterval(this.moving);
	this.moving = null;
}

Player.prototype.dropItems = function() {
	this.dropWeapon();
	this.dropValuableItem();
}

Player.prototype.dropWeapon = function() {
	if(this.weapon != null) {
		var auxCell = getCell(this.position);
		while(auxCell.element != null) {
			auxCell = auxCell.getRandomCell();
		}
		auxCell.setElement(this.weapon.name);
		this.weapon = null;
	}
}

Player.prototype.dropValuableItem = function() {
	var auxCell2 = getCell(this.position);
	if(this.valuableItem != null) {
		while(auxCell2.element != null) {
			auxCell2 = auxCell2.getRandomCell();
		}
		auxCell2.setElement(this.valuableItem.name);
		this.valuableItem = null;
	}
}

Player.prototype.hasNoWeapon = function() {
	return this.weapon == null || this.weapon.name == "Bomb";
}

Player.prototype.playSoundKill = function() {
	if(this.hasNoWeapon()) sample(elementtypes[searchElement("Baseball_Bat")].sounds).play();
	else if(this.weapon.sounds.length > 0) sample(this.weapon.sounds).play();
}

Player.prototype.draw = function()  {

	var myColor = ((this.invincible) ? "#" + Math.random().toString(16).slice(2, 8) : this.color);

	playercanvas.beginPath();
	playercanvas.fillStyle=myColor;
	var realsquare = cellSize-2;

	playercanvas.arc(this.position.x*cellSize+cellSizePart(0.5)+4, this.position.y*cellSize+cellSizePart(0.5)+4, cellSizePart(0.3), 0, 2*Math.PI);
	playercanvas.fill();

	playercanvas.beginPath();

	var rad;
	switch(this.lookDirection) {
		case WEST:
			rad = Math.PI/2;
			break;
		case NORTH:
			rad = Math.PI;
			break;
		case EAST:
			rad = Math.PI*3/2;
			break;
		default:
			rad = 0;
	}

	var transX = this.position.x*cellSize + cellSizePart(0.5) + 4;
	var transY = this.position.y*cellSize + cellSizePart(0.5) + 4;

	playercanvas.translate(transX, transY);
	playercanvas.rotate(rad);

	this.skin.drawShape(myColor);

	playercanvas.fillStyle="#F5D0A9";

	playercanvas.beginPath();

	// body
	playercanvas.rect(this.position.x*cellSize + cellSizePart(0.4) + 4 - transX, this.position.y*cellSize + cellSizePart(0.5) + 4 - transY, cellSizePart(0.2), cellSizePart(0.2));
	playercanvas.arc(this.position.x*cellSize + cellSizePart(0.4) + 4 - transX, this.position.y*cellSize + cellSizePart(0.6) + 4 - transY, cellSizePart(0.1), arcPosition(SOUTH), arcPosition(NORTH));
	playercanvas.arc(this.position.x*cellSize + cellSizePart(0.6) + 4 - transX, this.position.y*cellSize + cellSizePart(0.6) + 4 - transY, cellSizePart(0.1), arcPosition(NORTH), arcPosition(SOUTH));
	playercanvas.fill();

	//item
	if(this.valuableItem != null) {
		var img = $("#" + this.valuableItem.name)[0];
		playercanvas.drawImage(img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(0.35*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(0.35*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.1) - transY, cellSizePart(0.3), cellSizePart(0.3));
	}
	//dagger
	if(this.weapon != null && this.weapon.name == "Dagger") {
		var img = $("#Dagger")[0];
		drawImageRot(playercanvas, img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.15) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.02) - transY, cellSizePart(0.4), cellSizePart(0.4), 120);
	} 
	//sharpened dagger
	if(this.weapon != null && this.weapon.name == "Sharpened_Dagger") {
		var img = $("#Sharpened_Dagger")[0];
		drawImageRot(playercanvas, img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.1) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.1) - transY, cellSizePart(0.5), cellSizePart(0.5), 180);
	} 
	//sword
	else if(this.weapon != null && this.weapon.name == "Sword") {
		var img = $("#Sword")[0];
		drawImageRot(playercanvas, img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.25) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) + cellSizePart(0.03) - transY, cellSizePart(0.7), cellSizePart(0.7), 210);
	}
	//long sword
	else if(this.weapon != null && this.weapon.name == "Long_Sword") {
		var img = $("#Long_Sword")[0];
		drawImageRot(playercanvas, img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.2) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.05) - transY, cellSizePart(0.8), cellSizePart(0.8), 190);
	}
	//flaming blade
	else if(this.weapon != null && this.weapon.name == "Flaming_Blade") {
		var img = $("#Flaming_Blade")[0];
		drawImageRot(playercanvas, img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.25) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) + cellSizePart(0.03) - transY, cellSizePart(0.7), cellSizePart(0.7), 210);	
	}
	//baseball bat
	else if(this.weapon != null && this.weapon.name == "Baseball_Bat") {
		var img = $("#Baseball_Bat")[0];
		drawImageRot(playercanvas, img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.22) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.05) - transY, cellSizePart(0.5), cellSizePart(0.5), 210);	
	}
	//bomb
	else if(this.weapon != null && this.weapon.name == "Bomb") {
		var img = $("#Bomb")[0];
		playercanvas.drawImage(img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.2) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.05) - transY, cellSizePart(0.3), cellSizePart(0.3));
	}

	playercanvas.beginPath();
	playercanvas.fillStyle = myColor;
	// right hand
	playercanvas.arc((this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - transY,cellSizePart(0.1),0,2*Math.PI);
	// left hand
	playercanvas.arc((this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(0.35*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(0.35*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - transY,cellSizePart(0.1),0,2*Math.PI);			
	playercanvas.fill();

	//shield
	if(this.weapon != null && this.weapon.name == "Shield") {
		var img = $("#Shield")[0];
		playercanvas.drawImage(img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.25) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.15) - transY, cellSizePart(0.5), cellSizePart(0.5));
	}
	//huge shield
	else if(this.weapon != null && this.weapon.name == "Huge_Shield") {
		var img = $("#Huge_Shield")[0];
		playercanvas.drawImage(img, (this.position.x*cellSize + cellSizePart(0.5) + 4) + Math.sin(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.3) - transX,(this.position.y*cellSize + cellSizePart(0.5) + 4) + Math.cos(1.4*Math.PI + this.rightFoot*Math.PI*0.25)*cellSizePart(0.4) - cellSizePart(0.25) - transY, cellSizePart(0.8), cellSizePart(0.8));
	}

	// eyes
	playercanvas.beginPath();
	playercanvas.fillStyle="black";
	playercanvas.rect(this.position.x*cellSize + cellSizePart(0.4) + 4 - transX, this.position.y*cellSize + cellSizePart(0.55) + 4 - transY, cellSizePart(0.05), cellSizePart(0.1));
	playercanvas.rect(this.position.x*cellSize + cellSizePart(0.55) + 4 - transX, this.position.y*cellSize + cellSizePart(0.55) + 4 - transY, cellSizePart(0.05), cellSizePart(0.1));

	playercanvas.rotate(-rad);
	playercanvas.translate(-transX, -transY);

	playercanvas.fill();

	playercanvas.fillStyle = "black";
	playercanvas.font = Math.floor(cellSize/5) + "px Arial";
	playercanvas.fillText(this.name, this.position.x*cellSize  + cellSizePart(0.5) - Math.floor(playercanvas.measureText(this.name).width / 2), this.position.y*cellSize);

}

function cellSizePart(multiplier) {
	return cellSize*multiplier;
}

function arcPosition(dir) {
	switch(dir) {
		case NORTH:
			return 3*Math.PI/2;
		case SOUTH:
			return Math.PI/2;
		case WEST:
			return Math.PI;
		default:
			return 0;
	}
}

Player.prototype.chooseAction = function() {
	if(this.IA != undefined && this.IA != null) {
		switch(this.IA) {
			case "guardian":
				return this.guardianAction();
			case "defaultBot":
				return this.defaultBot();
			default:
				return null;
		}
	} else return null;
}

Player.prototype.defaultBot = function() {

	var room = this.castle.rooms[this.position.z];

	this.lastVisitedRoom = room.id;
	if(this.visitedRoomsIA.length == 0) this.visitedRoomsIA = [this.position.z];
	if(this.visitedRoomsIA[0] != room.id && this.visitedRoomsIA.indexOf(room.id) == -1) this.visitedRoomsIA.unshift(room.id);
	else while(this.visitedRoomsIA[0] != room.id && this.visitedRoomsIA.indexOf(room.id) > 0) this.visitedRoomsIA.shift();

	// If you're chasing someone, go on
	if(this.chasing != null && room.hasPlayer(this.chasing) && Math.random() < 0.99) {
		this.nextActions = [];
		return makeCellPath(room.getCell(this.position), room.getCell(this.chasing.position))[0];
	} else this.chasing = null;

	// If you're reacting to someone, go on
	if(this.reactionToCharacter.length > 0 && this.characterMadeReaction != null && room.hasPlayer(this.characterMadeReaction) && !this.characterMadeReaction.dead) {
		this.nextActions = [];
		return this.reactionToCharacter.shift();
	} else {
		this.reactionToCharacter = [];
		this.characterMadeReaction = null;
	}

	// If you have some interesting weapon in the floor, take it
	if(room.getCell(this.position).element != null && this.weapon != null && room.getCell(this.position).element.type.isWeapon != undefined && room.getCell(this.position).element.type.level > this.weapon.level) {
		return 4;
	}

	// If you're stealing, go on unless you have an item. Same if you are carrying an item and were changing your room or going base
	if(((this.valuableItem == null && this.stealingBase != null && this.stealingBase.type.vulnerable(this.castle.players, this.castle.playersForTeam)) || this.valuableItem != null) && this.nextActions.length > 0) {
		var look = this.lookForCharacters(7);
		if(look != null) return look;
	} else this.stealingBase = null;

	// If there is a naked base and you wear nothing... STEAL!!
	if(this.valuableItem == null && Math.random() < 0.9) {
		var nakedBases = room.nakedBases(this.team, this.castle);
		if(nakedBases.length > 0) {
			this.stealingBase = sample(nakedBases);
			this.nextActions = makeCellPath(room.getCell(this.position), room.getCell(this.stealingBase.position));
			return this.nextActions.shift();
		}
	}

	// If you have item and see base, go there
	if(this.valuableItem != null && room.id == this.base.z) {
		this.nextActions = makeCellPath(room.getCell(this.position), room.getCell(this.base));
		var look = this.lookForCharacters(5);
		if(look != null) return look;
	} 
	// If not, find a way to base
	else if(this.valuableItem != null) {
		var passages = room.getPassages();
		if(passages.length > 0) {
			if(this.visitedRoomsIA.length <= 1) {
				var chosen = sample(passages);
				this.nextActions = makeCellPath(room.getCell(this.position), room.getCell(chosen.position));
				this.nextActions.push(chosen.type.direction);
				return this.nextActions.shift();
			}
			else {
				for(var i in passages) {
					if(room.getRoom(passages[i].type.direction) != null && room.getRoom(passages[i].type.direction).id == this.visitedRoomsIA[1]) {
						this.nextActions = makeCellPath(room.getCell(this.position), room.getCell(passages[i].position));
						this.nextActions.push(passages[i].type.direction);
						return this.nextActions.shift();
					}
				}
			}
		}
	}

	// If you have a focused cell and that cell is still interesting, go on
	if(this.focusedCell != null && this.focusedCell.element != null && !this.position.equals(this.focusedCell.position) && this.nextActions.length > 0 && Math.random() < 0.99) {
		var look = this.lookForCharacters(7);
		if(look != null) return look;
	} else this.focusedCell = null;	

	// If you have no weapon, try to find one
	if(this.weapon == null && Math.random() < 0.7)	{
		var weapon = this.searchForWeapon(room);
		if(weapon != null) {
			this.focusedCell = getCell(weapon.position);
			this.nextActions = makeCellPath(room.getCell(this.position), this.focusedCell);
			return this.nextActions.shift();
		}
	}

	// If you have no item, try to find one
	if(this.valuableItem == null && Math.random() < 0.7) {
		var item = this.searchForItem(room);
		if(item != null) {
			this.focusedCell = getCell(item.position);
			this.nextActions = makeCellPath(room.getCell(this.position), this.focusedCell);
			return this.nextActions.shift();
		}
	}

	// If you were doing something, finish it
	if(this.nextActions.length > 0 && Math.random() < 0.99) {
		var character = this.checkCellPath(this.nextActions, 10);
		var look = this.lookForCharacters(10);
		if(look != null) return look;
	} else this.nextActions = [];

	// Go to another room
	var toRoom = this.goToAntoherRoom(room);
	if(toRoom != null && Math.random() <= 0.9) return toRoom;

	// Walk to random point
	var randomCell = sample(sample(room.cells));
	this.nextActions = makeCellPath(room.getCell(this.position), randomCell);
	return this.nextActions.shift();

}

Player.prototype.lookForCharacters = function(numOfCells) {
	//console.log(this.nextActions);
	if(numOfCells == undefined || numOfCells == null) numOfCells = 7;
	var character = this.checkAround(numOfCells);
	//console.log(character);
	if(character == null) return this.nextActions.shift();
	else {
		this.reactionToCharacter = this.reactToCharacter(character);
		if(this.reactionToCharacter.length > 0) return this.reactionToCharacter.shift();
		else return null;
	}
}

Player.prototype.searchForWeapon = function(room) {

	var interestingCells = room.strongestWeapons();

	if(interestingCells.length > 0 && (this.weapon == null || interestingCells[0].type.level > this.weapon.level)) return sample(interestingCells);
	return null;

}

Player.prototype.searchForItem = function(room) {

	var interestingCells = room.valuestItems();

	if(interestingCells.length > 0 && this.valuableItem == null) return sample(interestingCells);
	return null;

}

Player.prototype.goToAntoherRoom = function(room) {
	var possiblePassages = room.getPassages();

	if(possiblePassages.length > 0) {
		var passage;
		var notVisited = [];
		for(var i in possiblePassages) {
			if(this.visitedRoomsIA.indexOf(room.getRoom(possiblePassages[i].type.direction).id) == -1 && room.getRoom(possiblePassages[i].type.direction).id != this.lastVisitedRoom) notVisited.push(possiblePassages[i]);
		}
		if(notVisited.length > 0) passage = sample(notVisited);
		else {
			if(possiblePassages.length > 1 && this.lastVisitedRoom != null) {
				for(var i in possiblePassages) {
					if(room.getRoom(possiblePassages[i].type.direction).id == this.lastVisitedRoom) {
						possiblePassages.splice(i, 1);
						break;	
					} 
				}
			}
			passage = sample(possiblePassages);
		}

		this.nextActions = makeCellPath(room.getCell(this.position), room.getCell(passage.position));
		if(passage.type.direction != 4 && passage.type.direction != 5 && passage.type.direction != this.nextActions[this.nextActions.length-1]) this.nextActions.push(passage.type.direction);
		return this.nextActions.shift();
	}

	return null;
}

Player.prototype.checkCellPath = function(cellPath, numOfCells) {

	var myCell = getCell(this.position);

	for(var i = 0; i < cellPath.length && (numOfCells == null || i < numOfCells); i++) {
		if(myCell.getCell(cellPath[i]) != null) myCell = myCell.getCell(cellPath[i]);
		if(myCell.characterOverCell != null) {
			//console.log(this.name + ": " + myCell.characterOverCell.name);
			return myCell.characterOverCell;
		}
	}

	return null;

}

Player.prototype.checkAround = function(numOfCells, initialCellPath) {

	var myCell = getCell(this.position);
	if(initialCellPath == null || initialCellPath == undefined) initialCellPath = [NORTH, EAST, WEST, SOUTH];

	for(var i in initialCellPath) {
		var dir = initialCellPath[i];
		if(myCell.hasPath(dir)) {
			var character = this.checkAroundAux(oppositeDirection(dir), myCell.getCell(dir), numOfCells - 1);
			if(character != null) return character;
		}
	}

	return null;

}

Player.prototype.checkAroundAux = function(from, myCell, numOfCells) {

	if(myCell.characterOverCell != null && myCell.characterOverCell.team != this.team) return myCell.characterOverCell;
	if(numOfCells < 0) return null;

	for(var i = 0; i < 4; i++) {
		if(i != from && myCell.hasPath(i)) {
			var character = this.checkAroundAux(oppositeDirection(i), myCell.getCell(i), numOfCells - 1);
			if(character != null) return character;
		}
	}

	return null;

}

Player.prototype.reactToCharacter = function(character) {

	this.characterMadeReaction = character;
	if(character.team == this.team) {
		this.characterMadeReaction = null;
		return [];	
	} 

	var chance;
	if(!this.hasNoWeapon()) chance = this.weapon.chance(character.weapon);

	var room = this.castle.rooms[this.position.z];

	// Running away
	if(((this.hasNoWeapon() && !character.hasNoWeapon()) || (this.weapon != null && chance == 0) || character.invincible) && !this.invincible) {
		return this.runAway(character, true);
	} else if((this.weapon != null && chance <= 0.5 && Math.random() > chance) && !this.invincible) {
		return this.runAway(character, false);
	} else if(this.invincible || Math.random() <= chance) {
		this.chasing = character;
		this.characterMadeReaction = null;
		return makeCellPath(room.getCell(this.position), room.getCell(character.position));
	}
	else {
		this.characterMadeReaction = null;
		return [];
	}

}

Player.prototype.runAway = function(character, desperated) {

	var room = this.castle.rooms[this.position.z];

	// First, check if some weapon could help me
	var weapons = room.getWeapons();

	// Sort weapons
	weapons.sort(function(weapon1, weapon2) {
		var chance1 = weapon1.type.chance(character.weapon);
		var chance2 = weapon2.type.chance(character.weapon);
		if(chance2 - chance1 != 0) return chance2 - chance1;
		else return -1;
	});

	//console.log(weapons);

	var contador;
	// Check if there is an interesting weapon
	for(contador = 0; contador < weapons.length; contador++) {
		// Maybe the weapon is not good enough
		var chance = weapons[contador].type.chance(character.weapon);
		if(chance <= 0.5 && Math.random() > chance || (!this.hasNoWeapon() && this.weapon.chance(character.weapon) >= chance)) break;

		var cellPath = makeCellPath(room.getCell(this.position), room.getCell(weapons[contador].position));
		cellPath.push(4);
		if(this.checkCellPath(cellPath, 10) == null) return cellPath;
	}

	//console.log("1");
	//console.log(weapons);

	var passages = room.getPassages();
	var cellPaths = [];
	for(var j in passages) {
		cellPaths.push(makeCellPath(room.getCell(this.position), room.getCell(passages[j].position)));
		if(passages[j].type.direction < 4) cellPaths[j].push(passages[j].type.direction);
	}
	// The shorter, the better
	cellPaths.sort(function(cellpath1, cellpath2) {
		var length1 = cellpath1.length;
		var length2 = cellpath2.length;
		if(length1 - length2 != 0) return length1 - length2;
		else return length1;
	});
	for(var j in cellPaths) {
		if(this.checkCellPath(cellPaths[j], 15) == null) return cellPaths[j];
	}

	//console.log("2");
	//console.log(weapons);

	// Problem
	while(contador < weapons.length) {
		//console.log("contador: " + contador);
		//console.log(weapons[contador]);
		if(!this.hasNoWeapon() && this.weapon.chance(character.weapon) >= weapons[contador].type.chance(character.weapon)) break;
		var cellPath = makeCellPath(room.getCell(this.position), room.getCell(weapons[contador].position));
		cellPath.push(4);
		if(this.checkCellPath(cellPath, 10) == null) return cellPath;
		contador++;
	}

	if(desperated) return sample(room.getCell(this.position).paths);
	else {
		this.chasing = character;
		return makeCellPath(room.getCell(this.position), room.getCell(this.chasing.position));
	}

}

Player.prototype.guardianAction = function() {

	var room = this.castle.rooms[this.position.z];
	if(room.id != this.lastVisitedRoom) {
		this.lastVisitedRoom = room.id;
		this.guarding_time = 100;
	}
	this.guarding_time--;

	if(this.chasing != null && room.hasPlayer(this.chasing) && !this.chasing.dead) {
		this.nextActions = [];
		return makeCellPath(room.getCell(this.position), room.getCell(this.chasing.position))[0];
	} else this.chasing = null;

	if(this.weapon != null) {
		var weakest = room.weakestPlayers(this);
		if(weakest.length != 0) {
			this.chasing = sample(weakest);
			return makeCellPath(room.getCell(this.position), room.getCell(this.chasing.position))[0];
		}
	}

	// if there is focused cell and weapon still there, go
	if(this.focusedCell != null && this.focusedCell.element != null && this.focusedCell.element.type.isWeapon != undefined && this.focusedCell.position.z == this.position.z) {
		var cp = makeCellPath(room.getCell(this.position), this.focusedCell);
		if(cp.length == 0) {
			this.focusedCell = null;
			return 4;
		} else return cp[0];
	}
	else this.focusedCell = null;

	// if there is no focused weapon, look for one
	var interestingCells = room.strongestWeapons();

	if(interestingCells.length > 0 && (this.weapon == null || interestingCells[0].type.level > this.weapon.level)) {
		this.focusedCell = getCell(sample(interestingCells).position);
		return makeCellPath(room.getCell(this.position), this.focusedCell)[0];
	}

	////console.log("Baliza 5");

	// here next action if there is
	if(this.nextActions.length > 0) return this.nextActions.shift();

	var weakest = room.weakestPlayers(this);
	if(weakest.length != 0) {
		this.chasing = sample(weakest);
		return makeCellPath(room.getCell(this.position), room.getCell(this.chasing.position))[0];
	}

	// if couldnt find weapons and there is no action, just go another room
	var passages = room.getPassages();
	if(passages.length > 0 && this.guarding_time <= 0) {
		var passage = sample(passages);
		this.nextActions = makeCellPath(room.getCell(this.position), room.getCell(passage.position));
		if(passage.type.direction != 4 && passage.type.direction != 5 && passage.type.direction != this.nextActions[this.nextActions.length-1]) this.nextActions.push(passage.type.direction);
		return this.nextActions.shift();
	}

	////console.log("Baliza 7");

	// If cant, just walk to a random point
	var randomCell = sample(sample(room.cells));
	this.nextActions = makeCellPath(room.getCell(this.position), randomCell);
	return this.nextActions.shift();

}

Player.prototype.restartAction = function() {
	if(this.actions == null && im_owner) {
		var myBot = this;
		this.actions = setInterval(function() {
			var dir = myBot.chooseAction();
			if(one_player) myBot.castle.movePlayer(myBot, dir);
			else socket.emit("move", myBot.id, dir);
		}, MOVING_TIME + myBot.botDelay);
	}
}

Player.prototype.startAction = function() {
	if(this.actions == null && im_owner) {
		var myBot = this;
		setTimeout(function() {
			myBot.actions = setInterval(function() {
				var dir = myBot.chooseAction();
				if(one_player) myBot.castle.movePlayer(myBot, dir);
				else socket.emit("move", myBot.id, dir);
			}, MOVING_TIME + myBot.botDelay);
		}, randomNum(500,3000));
	}
}

Player.prototype.stopAction = function() {
	clearInterval(this.actions);
	this.actions = null;
}

Player.prototype.isSameTeam = function(anotherPlayer) {
	return this.team != null && anotherPlayer.team != null && anotherPlayer.team ==	this.team;
}

Player.prototype.stopFocusing = function() {
	this.focusedCell = null;
	this.chasing = null;
}

Player.prototype.toStats = function() {
	return "<tr><td><font color='" + this.color + "'><strong>" + this.name + "</strong></font></td><td>" + ((this.dead) ? " (DEAD)" : "") + "</td><td><strong>" + this.money + "$</strong></td><td><font color='blue'>" + this.kills + "</font></td><td><font color='red'>" + this.deads + "</font></td><td><font color='#cccc00'>" + this.moneyCollected + "$</font></td><td><font color='green'>" + this.steals + "</font></td><td><font color='purple'>" + ("" + this.luck).substr(0, 5) + "</font></td></tr>";
}