var id_count_road = 0;
function Road() {

	this.id = id_count_road++; // integer
	this.path = []; // array of directions
	this.unions = []; // array of RoadUnion
	this.cellCount = 0;

	this.color = "#" + Math.random().toString(16).slice(2, 8); // for debugging
	this.memory = [];

}

Road.prototype.getCode = function() {
	var res = this.id + "!";
	for(var i = 0; i < this.path.length; i++) {
		res += this.path[i]; // this doesnt need separator because its only a number between 0 and 3
	}
	res += "!";
	for(var i = 0; i < this.unions.length; i++) {
		res += this.unions[i].getCode();
		if(i < this.unions.length-1) res += "]";
	}
	return res + "!" + this.cellCount;
}

Road.prototype.toString = function() {
	return this.id;
}

Road.prototype.finishUnions = function(roads) {
	for(var i in this.unions) {
		for(var j in roads) {
			if(roads[j].id == this.unions[i].road_connected_num) this.unions[i].road_connected = roads[j];
		}
	}
}

function RoadUnion(my_position, road_connected, dir_connection) {

	this.my_position = my_position; // int type (number)
	this.road_connected = road_connected; // road type
	this.road_connected_num = road_connected.id;
	this.dir_connection = dir_connection; // direction where next road is

}

RoadUnion.prototype.getCode = function() {
	return this.my_position + "+" + this.road_connected_num + "+" + this.dir_connection;
}

Road.prototype.addUnion = function(my_position, road_connected, dir_connection) {
	this.unions.push(new RoadUnion(my_position, road_connected, dir_connection));
}

RoadUnion.prototype.toString = function() {
	return "{" + this.my_position + ", " + this.road_connected.id + ", " + this.dir_connection + "}";
}

function RoadPathMemory(road_target, road_path) {
	this.road_target = road_target; // id of the target we want to go
	this.road_path = road_path; // paths we have to follow to reach that road
}

RoadPathMemory.prototype.toString = function() {
	return this.road_target + ": " + this.road_path;
}

Road.prototype.findUnion = function(road_num) {
	for(var i in this.unions) {
		if(this.unions[i].road_connected.id == road_num) return this.unions[i];
	}
	return null;
}

/**
* This function will return the direction that someone in the cell "cell" has to follow to reach the road "otherRoad"
*/

Road.prototype.goToCellInRoad = function(cell_code, otherCell_code) {

	if(cell_code == otherCell_code) return [];
	else if(cell_code < otherCell_code) {
		var res = [];
		for(var i = cell_code; i < otherCell_code; i++) {
			res.push(this.path[i]);
		}
		return res;
	} else {
		var res = [];
		for(var i = cell_code-1; i >= otherCell_code; i--) {
			res.push(oppositeDirection(this.path[i]));
		}
		return res;
	}

}

// Recursive solution. Returns a list of ids of the roads that we have to pass to go from the road "this" to road "otherRoad"
Road.prototype.makeRoadPath = function(otherRoad) {
	for(var i in this.memory) {
		if(this.memory[i].road_target == otherRoad.id) return this.memory[i].road_path;
	}

	var res = this.makeRoadPathAux(otherRoad, null);
	this.memory.push(new RoadPathMemory(otherRoad.id, res));
	return res;
}

Road.prototype.makeRoadPathAux = function(otherRoad, father) {

	////console.log(this.id + " == " + otherRoad.id);
	if(this.id == otherRoad.id) {
		return [this];
	} else {
		for(var i in this.unions) {
			if(father != this.unions[i].road_connected.id) {
				var res = this.unions[i].road_connected.makeRoadPathAux(otherRoad, this.id);
				////console.log(" - " + res);
				if(res != null) {
					////console.log("Hi " + this);
					res.unshift(this);
					return res;
				}
			}
		}
		return null;
	}

}

Road.prototype.getNewCode = function() {
	return this.cellCount++;
}

Road.prototype.makeMemories = function(roads) {
	for(var i in roads) {
		this.makeRoadPath(roads[i]);
	}
}

function makeCellPath(cell, otherCell) {

	//console.log(cell.roomUp.id + " <> " + otherCell.roomUp.id);
	//console.log(cell);
	var roadPath = cell.road_belonged.makeRoadPath(otherCell.road_belonged);

	var cellPath = [];
	var cell_code = cell.road_code;

	for(var i = 0; i < roadPath.length; i++) {
		var cellPathAux;
		if(i == roadPath.length - 1) { // im in the last road. just gotta go to the cell
			cellPathAux = roadPath[i].goToCellInRoad(cell_code, otherCell.road_code);
		} else {
			var union = roadPath[i].findUnion(roadPath[i+1].id);
			cellPathAux = roadPath[i].goToCellInRoad(cell_code, union.my_position);
			cellPathAux.push(union.dir_connection);
			cell_code = roadPath[i+1].findUnion(roadPath[i].id).my_position;
		}

		for(var j in cellPathAux) {
			cellPath.push(cellPathAux[j]);
		}
	}

	return cellPath;

}