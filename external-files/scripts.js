const NORTH = 0, SOUTH = 1, EAST = 2, WEST = 3, UP = 4, DOWN = 5, DEADTIME = 10000, MOVING_TIME = 150;
var cellSize, canvasize;
var canvas, playercanvas;
var castillo;
var road_colors = false, tutorial_mode = false;
var tutorial_name;
var tutorial_index = 0, tutorial_aux;
var socket, user_name = null, one_player = true, im_owner = true;

function play() {
	tutorial_mode = false;
	$("#mainbox").hide();
	$("#castlebox").css("display", "inline-block");
	$("#stats_box").css("display", "inline-block");

	canvasize = Math.min($("#castlebox").height(), $("#castlebox").width());
	$("#castle")[0].width = canvasize;
	$("#castle")[0].height = canvasize;
	$("#player")[0].width = canvasize;
	$("#player")[0].height = canvasize;

	canvas = $("#castle")[0].getContext("2d");
	playercanvas = $("#player")[0].getContext("2d");

	castillo = new Castle("random");
	castillo.start();
}

function playCastle(size, levels, numberOfTeams, playersForTeam, playerlist, time, itemValue, weaponValue, numberOfGuardians, me) {
	tutorial_mode = false;
	$("#mainbox").hide();
	$("#castlebox").css("display", "inline-block");
	$("#stats_box").css("display", "inline-block");

	canvasize = Math.min($("#castlebox").height(), $("#castlebox").width());
	$("#castle")[0].width = canvasize;
	$("#castle")[0].height = canvasize;
	$("#player")[0].width = canvasize;
	$("#player")[0].height = canvasize;

	canvas = $("#castle")[0].getContext("2d");
	playercanvas = $("#player")[0].getContext("2d");

	castillo = new Castle(size, levels, numberOfTeams, playersForTeam, playerlist, time, itemValue, weaponValue, numberOfGuardians, me);
	castillo.start();
}

function prepareMultiplayerCastle(size, levels, numberOfTeams, playersForTeam, playerlist, time, itemValue, weaponValue, numberOfGuardians, me, seed) {
	tutorial_mode = false;
	one_player = false;
	$("#mainbox").hide();
	$("#castlebox").css("display", "inline-block");
	$("#stats_box").css("display", "inline-block");

	canvasize = Math.min($("#castlebox").height(), $("#castlebox").width());
	$("#castle")[0].width = canvasize;
	$("#castle")[0].height = canvasize;
	$("#player")[0].width = canvasize;
	$("#player")[0].height = canvasize;

	canvas = $("#castle")[0].getContext("2d");
	playercanvas = $("#player")[0].getContext("2d");

	$("#stats").html("Loading...");
	castillo = new Castle(size, levels, numberOfTeams, playersForTeam, playerlist, time, itemValue, weaponValue, numberOfGuardians, me, seed);
}

function tutorial() {
	tutorial_talking = [null];
	tutorial_name = generarNombres(1, 4, 9)[0];
	tutorial_mode = true;
	$("#mainbox").hide();
	$("#castlebox").css("display", "inline-block");
	$("#stats_box").css("display", "inline-block");
	$("#stats").css("height", "49%");
	$("#tutorial_text").html("Hello! My name is <strong>" + tutorial_name + "</strong> and I'm your new <strong>master</strong>. Follow my instructions.<p>First of all, try to move.<br>W: UP<br>S: DOWN<br>D: RIGHT<br>A: LEFT<p>SPACEBAR: Continue tutorial");
	$("#lasthit_box").hide();
	$("#tutorial_box").fadeIn(1000);

	canvasize = Math.min($("#castlebox").height(), $("#castlebox").width());
	$("#castle")[0].width = canvasize;
	$("#castle")[0].height = canvasize;
	$("#player")[0].width = canvasize;
	$("#player")[0].height = canvasize;

	canvas = $("#castle")[0].getContext("2d");
	playercanvas = $("#player")[0].getContext("2d");

	castillo = new Castle(10, 1, 1, 1, 1, 90000, "Coin|2,Book", "Sword,Dagger,Shield", 0);
	castillo.start();
}

function checkTutorial() {
	var player = castillo.players[0];
	var condition = false;
	switch(tutorial_index) {
		case 1:
			if(castillo.presentRoom.getCell(player.position).element == null || castillo.presentRoom.getCell(player.position).element.type.name != "Base0") {
				condition = true;
				$("#tutorial_text").html("Good! Now lets talk about your main mission. You have to collect as <strong>much items as possible</strong>. Easy, huh?<br>Each of those items have a different value. While coins are worth a dollar, books can give you three dollars!");
				tutorial_talking = ["Can you see <strong>this <font color='blue'>blue-framed</font> coin</strong>? Take it! Just go to its location. You may have to find the way in this labyrinthine castle<p>R: Drop item or switch it with floor's item."];
			}			
		break;
		case 4:
			if(player.valuableItem != null && player.valuableItem.name == "Coin" && player.position.x == tutorial_aux.position.x && player.position.y == tutorial_aux.position.y) {
				condition = true;
				$("#tutorial_text").html("Now bring it back to base.");
				for(var i in castillo.presentRoom.elements) {
					if(castillo.presentRoom.elements[i].type.name == "Base0") {
						tutorialSquare(castillo.presentRoom.elements[i].position);
						tutorial_aux = castillo.presentRoom.elements[i];
						break;
					}
				}				
			} else if(player.valuableItem != null) {
				condition = true;
				$("#tutorial_text").html("That wasn't the item that I told you. I said <strong>THIS one!</strong>");
				tutorialSquare(tutorial_aux.position);
				tutorial_talking = [];
				castillo.movePlayer(player, 5);
				tutorial_index -= 2;
			}
		break;
		case 6:
			if(tutorial_aux.type.items.length > 0) {
				condition = true;
				$("#tutorial_text").html("Now you have collected 1$. You can check it in stats:<p><strong>Money:</strong> the money you have in base.<br><font color='blue'><strong>Kills:</strong> the number of enemies you have killed.</font><br><font color='red'><strong>Deads:</strong> the number of times you have dead</font>");
				tutorial_talking = ["Your next task is taking a <strong>weapon</strong>. There are three important kinds of weapons", "Swords are good against daggers, but weak against shields.", "Daggers are good against shields, but weak against swords", "And finally, shields are good against swords, but weak against daggers.", "All these weapons are level 1 weapons, but there are stronger ones! You'll find out playing them playing the game. Come on! Go find some weapon."];
			}
		break;
		case 13:
			if(player.weapon != null) {
				condition = true;
				var weapon;
				if(player.weapon.name == "Sword") weapon = "Dagger";
				else if(player.weapon.name == "Shield") weapon = "Sword";
				else weapon = "Shield";
				castillo.addBotToRandom("Tutorial Bot", 99, new PlayerSkin(), "nothing", "guardian", elementtypes[searchElement(weapon)], null, 50);
				$("#tutorial_text").html("You're prepared to battle. Try to kill this guy! You will need two last tips:<br><strong>1. </strong>If you attack from <strong>behind</strong> you will 100% kill someone, no matter what weapon wears. Beware! They can kill you the same way.<br><strong>2.</strong> Attacking from the side will improve your chances to kill.<p>And a <strong>very last tiny thing</strong>: You have 90% to kill someone attacking face to face with a stronger weapon. Not 100%!<p>Good luck.");
				castillo.drawElements();
				tutorialSquare(castillo.players[1].position);
			}
		break;
		case 15:
			if(castillo.players[1].dead) {
				condition = true;
				$("#tutorial_text").html("BOOM! Good job!");
				tutorial_aux = player.weapon.name;
				tutorial_talking = ["Now, which weapon would you pick to defeat a bot with a " + tutorial_aux + "? Search it.<p><strong>Tip:</strong> you can drop a weapon or swap it with floor's one with <strong>E</strong>."];
			}
		break;
		case 18:
			if(player.weapon.strongAgainst.indexOf(tutorial_aux) > -1) {
				condition = true;
				$("#tutorial_text").html("Fine, you've the basics of combat. Now collect every item and bring them to base.");
				for(var i in castillo.presentRoom.elements) {
					if(castillo.presentRoom.elements[i].type.name == "Base0") {
						tutorial_aux = castillo.presentRoom.elements[i];
						break;
					}
				}
				tutorial_talking = [];
			}
		break;
		case 20:
			if(tutorial_aux.type.items.length == 3) {
				condition = true;
				$("#tutorial_text").html("Perfect. Look at this new enemy! It has its own base. Use your skills to kill it. But be careful! It has a weapon of <strong>level 2</strong>!");
				castillo.addBot("Tutorial Bot v2.0", new Position(0,0,0), 2, new PlayerSkin(), "nothing", "defaultBot", elementtypes[searchElement(sample(["Long_Sword","Huge_Shield","Sharpened_Dagger"]))], null, 150);
				elementtypes.push(new Base(2, castillo.players[2]));
				var pos;
				do {
					pos = new Position(randomNum(1,castillo.width-2), randomNum(1,castillo.height-2), 0);
					var err = castillo.addElement(pos, "Base2");
				} while(err);

				for(var i in castillo.presentRoom.elements) {
					if(castillo.presentRoom.elements[i].type.name == "Base2") {
						tutorial_aux = castillo.presentRoom.elements[i];
						tutorial_aux.type.items = [elementtypes[searchElement("Chandelier")]];
						break;
					}
				}
				castillo.players[2].position.x = pos.x;
				castillo.players[2].position.y = pos.y;
				castillo.players[2].position.z = pos.z;
				castillo.players[2].base = pos;
				castillo.rooms[0].getCell(new Position(0,0)).characterOverCell = null;
				castillo.drawElements();
				tutorialSquare(pos);
			}
		break;
		case 22:
			if(castillo.players[2].dead) {
				condition = true;
				$("#tutorial_text").html("<strong>WELL DONE!!!</strong><p>Look at that base! Its outline is <font color='red'>red</font>. That means that the base is <strong>vulnerable</strong>. You can go and steal some item!");
				tutorial_talking = [];
			}
		break;
		case 24:
			if(player.valuableItem != null && player.valuableItem.name == "Chandelier") {
				condition = true;
				$("#tutorial_text").html("You've got a beautiful chandelier that is worth 5$. Look at the stats! You've 1 <font color='green'><strong>steal</strong></font>. This stat counts the number of times you have stolen items.");				
				for(var i in castillo.presentRoom.elements) {
					if(castillo.presentRoom.elements[i].type.name == "Base0") {
						tutorial_aux = castillo.presentRoom.elements[i];
						break;
					}
				}
				tutorial_talking = ["<font color='#cccc00'><strong>Money collected</strong></font> counts the number of money you have brought base, even if they've been stolen.<br>The only stat that <strong>really counts</strong> is <strong>Money</strong>, but the others are just for knowing how good you played. <p>Well, carry that chandelier to base! " + ((tutorial_aux.type.items.length < 3) ? "And recover everything that bot has stolen to you..." : "")];	
			}	
		break;
		case 27:
			if(tutorial_aux.type.items.length == 4) {
				condition = true;
				$("#tutorial_text").html("There is a game timer. In normal mode, when time is out, the teams that have the most valuable items in base (the team that has more <strong>Money</strong>), <strong>wins</strong>.");
				tutorial_talking = ["I have to tell you something else. This is a <strong>passage</strong>. They're in form of doors or stairways. They transport you to another room. Come on! Try to use one of those."];
			}
		break;
		case 30:
			if(player.position.z == 1) {
				condition = true;
				$("#tutorial_text").html("Here we are! And I was hidden here all this time. Try to defeat me, steal my <strong>teddy</strong> (10$) and preserve your own items to finish this tutorial.<p>And... sorry, i'm cheating. I'm using the <strong>Flaming Blade</strong>. It has 75% to win <strong>any</strong> weapon of level 2. Ha ha ha!");

				castillo.addBot(tutorial_name, new Position(0,0,0), 3, new PlayerSkin(), "revive_base", "defaultBot", elementtypes[searchElement("Flaming_Blade")], null, 50);
				elementtypes.push(new Base(3, castillo.players[3]));
				var pos;
				do {
					pos = new Position(randomNum(1,castillo.width-2), randomNum(1,castillo.height-2), 1);
					var err = castillo.addElement(pos, "Base3");
				} while(err);

				for(var i in castillo.rooms[1].elements) {
					console.log(castillo.rooms[1].elements[i]);
					if(castillo.rooms[1].elements[i].type.name == "Base3") {
						tutorial_aux = castillo.rooms[1].elements[i];
						tutorial_aux.type.items = [elementtypes[searchElement("Teddy")]];
						break;
					}
				}
				castillo.players[3].position.x = pos.x;
				castillo.players[3].position.y = pos.y;
				castillo.players[3].position.z = pos.z;
				castillo.players[3].base = pos;
				castillo.rooms[1].getCell(new Position(0,0)).characterOverCell = null;
				castillo.drawElements();

				for(var i in castillo.rooms[0].elements) {
					if(castillo.rooms[0].elements[i].type.name == "Base0") {
						tutorial_aux = castillo.rooms[0].elements[i];
						break;
					}
				}
			}
		break;
		case 32:
			if(tutorial_aux.type.items.length == 5 || tutorial_aux.type.items.length == 0) {
				condition = true;
				castillo.time = 1;
				if(tutorial_aux.type.items.length == 5) $("#tutorial_text").html("Well, you won this tutorial. Congratulations.<p>One last thing: I have never helped you. You dont even know my name. Good luck.");
				else $("#tutorial_text").html("Well, you haven't past this tutorial, but you have learnt the basics.<p>One last thing: I have never helped you. You dont even know my name. Good luck.");
				tutorial_talking = [];
			}
		break;
		default:
	}

	if(condition) {
		tutorial_talking.push(null);
		$("#tutorial_box").fadeIn(500);
		tutorial_index++;
	}
}

function talkTutorial() {
	if($("#tutorial_square").css("display") != "none") $("#tutorial_square").fadeOut(500);
	if(tutorial_talking[0] == null) {
		$("#tutorial_box").fadeOut(500);
		tutorial_talking = [];
	}
	else {
		$("#tutorial_text").html(tutorial_talking.shift());
	}

	switch(tutorial_index) {
		case 2:
			for(var i in castillo.presentRoom.elements) {
				if(castillo.presentRoom.elements[i].type.name == "Coin") {
					tutorial_aux = castillo.presentRoom.elements[i];
					break;
				}
			}
			tutorialSquare(tutorial_aux.position);
		break;
		case 8:
			for(var i in castillo.presentRoom.elements) {
				if(castillo.presentRoom.elements[i].type.name == "Sword") {
					tutorial_aux = castillo.presentRoom.elements[i];
					break;
				}
			}
			tutorialSquare(tutorial_aux.position);
		break;
		case 9:
			for(var i in castillo.presentRoom.elements) {
				if(castillo.presentRoom.elements[i].type.name == "Dagger") {
					tutorial_aux = castillo.presentRoom.elements[i];
					break;
				}
			}
			tutorialSquare(tutorial_aux.position);
		break;
		case 10:
			for(var i in castillo.presentRoom.elements) {
				if(castillo.presentRoom.elements[i].type.name == "Shield") {
					tutorial_aux = castillo.presentRoom.elements[i];
					break;
				}
			}
			tutorialSquare(tutorial_aux.position);
		break;
		case 14:
			castillo.players[1].startAction();
		break;
		case 21:
			castillo.players[2].startAction();
		break;
		case 28:
			castillo.rooms.push(new Room(castillo.height, castillo.width, castillo.cellSize));
			var dir = randomNum(0,3);
			castillo.rooms[0].adjacentRooms[dir] = castillo.rooms[1];
			castillo.rooms[1].adjacentRooms[oppositeDirection(dir)] = castillo.rooms[0];
			var passPosition;

			do {
				passPosition = castillo.getPassagePositions(dir, castillo.rooms[0]);
				var err = castillo.addElement(passPosition[0], "passage" + dir);
				if(!err) {
					err = castillo.addElement(passPosition[1], "passage" + oppositeDirection(dir));
					if(err) castillo.removeElement(passPosition[0]);
				}
			} while(err);

			castillo.rooms[1].finishWalls(sample(sample(castillo.rooms[1].cells)));
			castillo.drawElements();
			tutorialSquare(passPosition[0]);
		break;
		case 31:
			castillo.players[3].startAction();
		break;
		default:
		console.log("talk: " + tutorial_index);
	}

	tutorial_index++;
}

function tutorialSquare(pos) {
	$("#tutorial_square").css("left", (4 + $("#castle").offset().left + (cellSize*pos.x)) + "px");
	$("#tutorial_square").css("top", (4 + $("#castle").offset().top + (cellSize*pos.y)) + "px");
	$("#tutorial_square").fadeIn(500);
}

$(document).keydown(function(e) {
	if(castillo != undefined) {
		var code = e.keyCode || e.which;

		var player = castillo.me;
		var dir = player.move(code);

		if(dir > -1 && dir < 4 && player.movingDirection.indexOf(dir) == -1) {
			if(player.moving != null) player.stopMoving();
			player.movingDirection.unshift(dir);
			if(one_player) castillo.movePlayer(player, dir);
			else socket.emit("move", player.id, dir);
		 	player.startMoving();
		} else if(dir >= 4) {
			if(one_player) castillo.movePlayer(player, dir);
			else socket.emit("move", player.id, dir);
		}
		if(tutorial_mode && tutorial_talking.length > 0 && code == 32) {
			talkTutorial();
		}
	}
});

$(document).keyup(function(e) {
	if(castillo != undefined) {
		var code = e.keyCode || e.which;

		var player = castillo.me;
		var dir = player.move(code);

		if(dir > -1 && dir < 4) {
			var pos = player.movingDirection.indexOf(dir);
			if(pos != -1) {
				player.movingDirection.splice(pos, 1);
				if(pos == 0) {
					player.stopMoving();
					if(player.movingDirection.length > 0) player.startMoving();
				}
			}
		}
	}
});

$(document).ready(function() {
	backgroundDark(false);
	$("#login_cancel").on("click", function() {
		backgroundDark(false);
		$("#login_box").hide();
	});
	$("#signup_cancel").on("click", function() {
		backgroundDark(false);
		$("body").css("background-color", "white");
		$("#signup_box").hide();
	});
});

function backgroundDark(turnOn) {
	if(turnOn) {
		$("body").css("background-color", "#424242");
		$("#login").off();
		$("#signup").off();
		$("#guest").off();
		$("#tutorial_button").off();
		$("#login").addClass("btn_no_hover");
		$("#login").removeClass("btn");
		$("#signup").addClass("btn_no_hover");
		$("#signup").removeClass("btn");
		$("#guest").addClass("btn_no_hover");
		$("#guest").removeClass("btn");
		$("#tutorial_button").addClass("btn_no_hover");
		$("#tutorial_button").removeClass("btn");
	} else {
		$("body").css("background-color", "white");
		$("#login").on("click", function() {
			backgroundDark(true);
			$("#login_box").show();
			$("#login_username").focus();
		});
		$("#signup").on("click", function() {
			backgroundDark(true);
			$("#signup_box").show();
			$("#signup_username").focus();
		});
		$("#guest").on("click", function() {
			if(socket == undefined || socket == null) {
				play();	
				//playCastle(10, 2, 1, 1, [["Yo"]], 1000, 10, 10, 3, "Yo");
			} 
			else {
				socket.emit('guest');
			}
		});
		$("#tutorial_button").on("click", function() {
			tutorial();
		});
		$("#login").addClass("btn");
		$("#login").removeClass("btn_no_hover");
		$("#signup").addClass("btn");
		$("#signup").removeClass("btn_no_hover");
		$("#guest").addClass("btn");
		$("#guest").removeClass("btn_no_hover");
		$("#tutorial_button").addClass("btn");
		$("#tutorial_button").removeClass("btn_no_hover");
	}
}

function Position(x, y, z) {
	if(typeof(x) == "object") {
		this.x = x.x;
		this.y = x.y;
		this.z = x.z;
	} else {
		if(arguments.length >= 2) {
			this.x = x;
			this.y = y;
		}
		if(arguments.length == 3) this.z = z;
		else this.z = null;
	}
}

Position.prototype.differences = function(another_position) {
	return Math.sqrt(Math.pow(this.x - another_position.x, 2) + Math.pow(this.y - another_position.y, 2));
}

Position.prototype.toString = function() {
	return "(" + this.x + "," + this.y + "," + this.z + ")";
}

Position.prototype.equals = function(position) {
	return this.x === position.x && this.y === position.y && this.z === position.z;
}

Position.prototype.getCode = function() {
	return this.x + "," + this.y;
}

function randomNum (min, max) {
	return Math.floor(Math.random() * (max-min+1) + min);
}

function isInArray (elem, array) {
	var res = false;
	for(var i in array) {
		if(elem.equals(array[i])) res = true;
	}
	return res;
}

function getWidthOfText(txt, fontsize){

	$("#widthGetter").css("font-size", fontsize  + "px");

	$("#widthGetter")[0].innerHTML = txt;

	var aux = $("#widthGetter").width();

	$("#widthGetter")[0].innerHTML = "";

  	return aux;

}

function oppositeDirection(dir) {
	switch(dir) {
		case NORTH:
			return SOUTH;
		case SOUTH:
			return NORTH;
		case EAST:
			return WEST;
		case WEST:
			return EAST;
		case UP:
			return DOWN;
		case DOWN:
			return UP;
		default:
			return -1;
	}
}

function sample(array) {
	var aux = Math.floor(Math.random()*array.length);
	return array[aux];
}

function getCell(pos) {
	return castillo.rooms[pos.z].getCell(new Position(pos.x, pos.y));
}

function turnArrayToStringDirs(array) {
	var dirs = ["N", "S", "E", "W", "UP", "DOWN"];
	var res = [];

	for(var i in array) {
		if(array[i] < dirs.length) res[i] = dirs[array[i]];
		else res[i] = "NO_DIRECTION";
	}

	return res;
}