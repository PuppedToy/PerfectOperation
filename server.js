var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var namesgen = require('namesgen.js');
//var castleMaker = require('castle.js');

app.get('/', function(req, res){
  res.sendfile('index.html');
});

var users = [], games = [];
var game_id_count = 0;

setInterval(function() {
  io.emit('check');
}, 15000);

io.on('connection', function(socket){
  var user_name = null, user_game = null, swapping = null;
  
  socket.on('guest', function() {
      if(user_name == null) {
          var name;
          do {
              name = "Guest " + namesgen.generarNombres(1, 4, 9)[0];
          } while(users.indexOf(name) > -1);
          users.push(name);
          if(user_name == null) {
              users.sort();
              socket.emit('first_login', name, users, games);
              socket.broadcast.emit('login', name);
              console.log(name + ' has connected.');
          }
          user_name = name;
      }
  });
  
  socket.on('logout', function() {
      if(user_name != null && users.indexOf(user_name) > -1) users.splice(users.indexOf(user_name), 1);
      console.log(user_name + " has logged out.");
      io.emit('logout', user_name);
      disconnectFromGame(user_name, user_game);
      user_name = null;
  });
  
  socket.on('disconnect', function() {
      if(user_name != null && users.indexOf(user_name) > -1) {
        users.splice(users.indexOf(user_name), 1);
        console.log(user_name + " has disconnected.");
        io.emit('logout', user_name);
        disconnectFromGame(user_name, user_game);
      }
  });
  
  socket.on('create_game', function(game) {
    if(!existsGame(game)) {
      game.id = game_id_count++;
      if(game_id_count > 29999) game_id_count = 0;
      games.push(game);
      socket.emit('create_game_ack', game);
      io.emit('create_game', game);
      user_game = game.id;
    }
  });
  
  socket.on('join_game', function(game) {
    
    var myGame = selectGameById(game);
    
    if(myGame.active) {
      socket.emit('join_game_started_ack');
      return;
    }
    
    if(myGame != null && !fullGame(myGame)) {
      user_game = game;
      var myGame = selectGameById(game);
      addPlayerToGame(user_name, myGame);
      io.emit('update_game_players', myGame);
      socket.emit('join_game_ack', myGame);
    } else if(myGame != null) {
      socket.emit('join_game_full_ack');
    }
    
  });
  
  socket.on('add_bot', function() {
    
    var myGame = selectGameById(user_game);
    
    if(!fullGame(myGame)) {
    
      do {
        var newBot = "Bot " + namesgen.generarNombres(1, 4, 9)[0];
      } while(existsUserInGame(newBot, myGame));
      
      addPlayerToGame(newBot, myGame);
      io.emit('update_game_players', myGame);
    
      ready(newBot, myGame);
    
    }
    
  });
  
  socket.on('leave_game', function() {
      disconnectFromGame(user_name, user_game);
      socket.emit('leave_game_ack');
      user_game = null;
  });
  
  socket.on('ready', function() {
    var myGame = selectGameById(user_game);
    ready(user_name, myGame);
    socket.emit('ready_ack');
  });
  
  socket.on('answer_castle', function(castle) {
    console.log("answered castle seed " + castle);
    var myGame = selectGameById(user_game);
    myGame.castle = castle;
    io.emit('start_multiplayer_game', myGame, castle);
  });
  
  socket.on('loaded_game', function() {
    var myGame = selectGameById(user_game);
    if(myGame.loaded.indexOf(user_name) == -1) myGame.loaded.push(user_name);
    if(myGame.loaded.length == (myGame.teams * myGame.nplayers)) {
      console.log("starting game");
      io.emit('loaded', myGame.id);
    }
  });
  
  socket.on('move', function(player_id, dir) {
    io.emit('move', user_game, player_id, dir, Math.random());
  });
  
  socket.on('owner', function(new_owner) {
    var myGame = selectGameById(user_game);
    myGame.owner = new_owner;
    io.emit('update_game_players', myGame);
  });
  
  socket.on('admin', function(new_admin) {
    var myGame = selectGameById(user_game);
    admin(new_admin, myGame);
  });
  
  socket.on('swap', function(team, id) {
    var myGame = selectGameById(user_game);
    swap(user_name, [team, id], myGame);
  });
  
  socket.on('chat_send', function(chat_room, msg) {
      io.emit('chat_send', "<strong><font color='#C8FFFC'>" + user_name + "</font></strong>: " + msg + "<br>");
  });
  
  socket.on('check', function(){});
  
});

http.listen(process.env.PORT, function(){
  console.log('listening on ' + process.env.PORT);
});

function existsGame(game) {
  for(var i in games) {
    if(games[i].id == game.id) return true;
  }
  return false;
}

function selectGameById(id) {
  for(var i in games) {
    if(games[i].id == id) return games[i];
  }
  return null;
}

function removeGame(game) {
  for(var i in games) {
    if(games[i].id == game.id) {
      games.splice(i, 1);
      return;
    }
  }
}

function ready(name, game) {
  if(game.readyplayers.indexOf(name) > -1) {
    game.readyplayers.splice(game.readyplayers.indexOf(name), 1);
  } else {
    game.readyplayers.push(name);
  }
  io.emit('ready', game);
  
  var nplayers = game.teams * game.nplayers;
  
  if(game.readyplayers.length == nplayers) {
    io.emit('start_game_alert', 5, game);
    setTimeout(function() {
      if(game.readyplayers.length == nplayers) {
        io.emit('start_game_alert', 4, game);
        setTimeout(function() {
          if(game.readyplayers.length == nplayers) {
            io.emit('start_game_alert', 3, game);
            setTimeout(function() {
              if(game.readyplayers.length == nplayers) {
                io.emit('start_game_alert', 2, game);
                setTimeout(function() {
                    if(game.readyplayers.length == nplayers) {
                      io.emit('start_game_alert', 1, game);
                      setTimeout(function() {
                          if(game.readyplayers.length == nplayers) {
                            startGame(game);
                          } else io.emit('cancel_start_game', game);
                      }, 1000)
                    } else io.emit('cancel_start_game', game);
                }, 1000);
              } else io.emit('cancel_start_game', game);
            }, 1000);
          } else io.emit('cancel_start_game', game);
        }, 1000);
      } else io.emit('cancel_start_game', game);
    }, 1000);
  }
}

function startGame(game) {
  var count = 0;
  for(var i in game.players) {
    for(var j in game.players[i]) {
      if(game.players[i][j].indexOf("Bot ") != 0) {
        count++;
      } else game.loaded.push(game.players[i][j]);
    }
  }
  
  if(count == 1) {
    for(var i in games) {
      if(games[i].id == game.id) {
        games.splice(i, 1);
        break;
      }
    }
    
    io.emit('start_individual_game', game);
  }
  else {
    for(var i in games) {
      if(games[i].id == game.id) {
        games[i].active = true;
        io.emit('refresh_game_activity', games[i].id);
        break;
      }
    }
    
    console.log("requesting_castle");
    io.emit('request_castle', game);
  }
}

function admin(name, game) {
  if(game.admins.indexOf(name) > -1) {
    game.admins.splice(game.admins.indexOf(name), 1);
  } else {
    game.admins.push(name);
  }
  console.log("Hello admin " + name + ": " + game.admins);
  io.emit('admin', game);
}

function swap(name_from, target, game) {
  var name_target = game.players[target[0]][target[1]];
  if(name_target == null || name_target.indexOf("Bot ") == 0 || game.swapping.indexOf(name_target + "-" + name_from) > -1) {
    var coord = [];
    for(var i in game.players) {
      for(var j in game.players[i]) {
        if(game.players[i][j] == name_from) {
          coord[0] = i;
          coord[1] = j;
        }
      }
    }
    
    game.players[coord[0]][coord[1]] = name_target;
    game.players[target[0]][target[1]] = name_from;
    
    io.emit("update_game_players", game);
    if(game.swapping.indexOf(name_target + "-" + name_from) > -1) {
      game.swapping.splice(name_target + "-" + name_from, 1);
      io.emit("swap_finish", game);
    }
    
  } else {
    game.swapping.push(name_from + "-" + name_target);
    io.emit('swap_request', name_target, name_from, game);
  }
}

function removePlayerFromGame(player, game) {
  var count = 0;
  var possible_owner = null;
  
  for(var i in game.players) {
    for(var j in game.players[i]) {
      if(player != null && player != null && player == game.players[i][j]) {
        game.players[i][j] = null;
      }
      if(game.players[i][j] != null && game.players[i][j].indexOf("Bot ") != 0) {
        if(possible_owner == null) possible_owner = game.players[i][j];
        count++;
      }
    }
  }
  
  if(game.owner == player && possible_owner != null) game.owner = possible_owner;
  
  return count;
}

function addPlayerToGame(player, game) {
  for(var i in game.players) {
    for(var j in game.players[i]) {
      if(player != null && game.players[i][j] == null) {
        game.players[i][j] = player;
        return;
      }
    }
  }
}

function disconnectFromGame(name, game) {
 var myGame = selectGameById(game);
  if(myGame != null) {
    var count = removePlayerFromGame(name, myGame);
    if(count == 0) {
      removeGame(myGame);
      io.emit('end_game', myGame);
    }
    else {
      io.emit('update_game_players', myGame);
    }
  } 
}

function existsUserInGame(name, game) {
  
  for(var i in game.players) {
    if(game.players[i].indexOf(name) > -1) return true;
  }
  
  return false;
  
}

function fullGame(game) {
  for(var i in game.players) {
    for(var j in game.players[i]) {
      if(game.players[i][j] == null) return false;
    }
  }
  return true;
}